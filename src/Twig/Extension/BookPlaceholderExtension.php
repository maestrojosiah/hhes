<?php 

namespace App\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Book;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class BookPlaceholderExtension extends AbstractExtension
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Environment $twig
    ) {}

    public function getFilters(): array
    {
        return [
            // Define the filter for rendering book placeholders
            new TwigFilter('render_books', [$this, 'renderBooks'], ['is_safe' => ['html']]),
        ];
    }

    public function renderBooks(string $content): string
    {
        preg_match_all('/\{\{([a-z0-9\-]+)\}\}/', $content, $matches);
        $slugs = $matches[1] ?? [];
    
        if (empty($slugs)) {
            return $content;
        }
    
        $books = $this->entityManager->getRepository(Book::class)->findBy(['slug' => $slugs]);
    
        $slugToHtml = [];
        foreach ($books as $book) {
            $slugToHtml[$book->getSlug()] = $this->twig->render('index/_book_card.html.twig', ['book' => $book]);
        }
    
        foreach ($matches[0] as $index => $placeholder) {
            $slug = $matches[1][$index];
            $replacement = $slugToHtml[$slug] ?? ''; // Empty if no matching book found
            $content = str_replace($placeholder, $replacement, $content);
        }
    
        return $content;
    }
    
}
