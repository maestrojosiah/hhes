<?php

namespace App\Twig;

use App\Controller\Admin\DashboardController;
use App\Manager\CartManager;
use App\Repository\BlogRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Repository\BookRepository;
use App\Repository\PageRepository;
use App\Repository\SEORepository;
use App\Repository\SettingRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GlobalVars extends AbstractExtension
{

    public function __construct(
        private CategoryRepository $categoryRepo,
        private UserRepository $usr,
        private BookRepository $bookRepo,
        private CartManager $cartManager,
        private Security $security,
        private RequestStack $requestStack,
        private AdminUrlGenerator $adminUrlGenerator, 
        private PageRepository $pageRepository, 
        private SettingRepository $settingRepo, 
        private SEORepository $seoRepo, 
        private BlogRepository $blogRepository,
    ) {
    }


    public function getFunctions(): array
    {
        return [new TwigFunction('global_vars', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {

        $books = $this->bookRepo->findBy(['deleted' => false]);
        $categories = $this->categoryRepo->findAll();
        $user = $this->getUserFromSecurityContext();
        $vce = $this->usr->findOneByUsername($this->getAgentUsernameFromCookie());
        if(null == $this->getAgentUsernameFromCookie() || $vce->getCategory() !== 'le'){
            $vce = $this->usr->findOneBy(
                ['usertype' => 'admin'],
                ['id' => 'asc']
            );
        }

        $url = parse_url((string)$_SERVER['REQUEST_URI']);
        $seo = $this->seoRepo->findAll();
        $setting = $this->settingRepo->findAll();
        $user = $this->getUserFromSecurityContext();
        $blog_posts = $this->blogRepository->findPublished(true, 3);
        $pages = $this->pageRepository->findByIsService(true);
    
        $glVars = [];
        $glVars['seo'] = !empty($seo) ? $seo[0] : null;
        $glVars['setting'] = !empty($setting) ? $setting[0] : null;
        $glVars['posts'] = $blog_posts;
        $glVars['pages'] = $pages;
        $glVars['books'] = $books;
        $glVars['categories'] = $categories;
        $glVars['cart'] = $this->cartManager->getCurrentCart($user);
        $glVars['vce'] = $vce;

        if ($user && $this->security->isGranted('ROLE_ADMIN')) {
            // Add edit links for admins
            if(!empty($seo)){
                $glVars['seo_edit_link'] = sprintf(
                    '<a target="_blank" href="%s"><i><img style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\SEOCrudController') // Adjust controller accordingly
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($seo[0]->getId()) // Assuming the first SEO entity
                        ->generateUrl()
                );
    
            }
            if(!empty($setting)){
                $glVars['setting_edit_link'] = sprintf(
                    '<a target="_blank" href="%s"><i><img style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\SettingCrudController') // Adjust controller accordingly
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($setting[0]->getId()) // Assuming the first Setting entity
                        ->generateUrl()
                );
            }
            
        }

        // $glVars['thisUrl'] = $url['path'];

        return $glVars[$select];
    }

    /**
     * Gets the agent's username from the cookie.
     *
     * @return string|null
     */
    private function getAgentUsernameFromCookie(): ?string
    {
        $request = $this->requestStack->getCurrentRequest();
        return $request->cookies->get('agent_username');
    }

    /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }
}
