<?php

namespace App\Twig;

use App\Controller\Admin\DashboardController;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Repository\ContentRepository;
use App\Repository\ImageRepository;
use Twig\Environment;
use Symfony\Bundle\SecurityBundle\Security;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class Render extends AbstractExtension
{
    public function __construct(
        private ContentRepository $contentRepo,
        private ImageRepository $imageRepo,
        private Environment $twig,
        private Security $security,
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('render_content', [$this, 'renderContent'], ['is_safe' => ['html']]),
            new TwigFunction('render_link_href', [$this, 'renderLinkHref'], ['is_safe' => ['html']]),
            new TwigFunction('render_link_content', [$this, 'renderLinkContent'], ['is_safe' => ['html']]),
            new TwigFunction('render_image', [$this, 'renderImage'], ['is_safe' => ['html']]),
            new TwigFunction('render_background_image', [$this, 'renderBackgroundImage'], ['is_safe' => ['html']]), // Background image function
        ];
    }

    public function renderContent(string $slug, string $default): string
    {
        $content = $this->contentRepo->findOneByDescription($slug);
    
        if ($content && !empty($content->getContentText())) {
            $renderedContent = $this->twig->createTemplate($content->getContentText())->render();
    
            $editLink = '';
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $editLink = sprintf(
                    '<a onclick="openModal(event, this)" href="%s"><i><img class="img-fluid" style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\ContentCrudController')
                        ->setAction('edit')
                        ->setDashboard(DashboardController::class)
                        ->setEntityId($content->getId())
                        ->generateUrl()
                );
            }
    
            return $renderedContent . ' ' . $editLink;
        }
    
        // If no content found, return default and provide a link to create a new entry
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $createLink = sprintf(
                '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/plus-icon.png" alt="edit"></i></a>',
                $this->adminUrlGenerator
                    ->setController('App\\Controller\\Admin\\ContentCrudController')
                    ->setDashboard(DashboardController::class)
                    ->setAction('new')
                    ->set('slug', $slug) // Pre-fill the slug field
                    ->set('contentText', $default) // Optionally pre-fill the content text
                    ->generateUrl()
            );
            return $default . ' ' . $createLink;
        }
    
        return $default;
    }
    
    public function renderLinkContent(string $slug, string $default, bool $adminLink = true): string
    {
        $content = $this->contentRepo->findOneByDescription($slug);
    
        if ($content && !empty($content->getContentText())) {
            $renderedContent = $this->twig->createTemplate($content->getContentText())->render();
    
            $editLink = '';
            if ($adminLink && $this->security->isGranted('ROLE_ADMIN')) {
                $editLink = sprintf(
                    '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\ContentCrudController')
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($content->getId())
                        ->generateUrl()
                );
            }
    
            return $renderedContent . ' ' . $editLink;
        }
    
        if ($adminLink && $this->security->isGranted('ROLE_ADMIN')) {
            $createLink = sprintf(
                '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/plus-icon.png" alt="edit"></i></a>',
                $this->adminUrlGenerator
                    ->setController('App\\Controller\\Admin\\ContentCrudController')
                    ->setDashboard(DashboardController::class)
                    ->setAction('new')
                    ->set('slug', $slug)
                    ->set('contentText', $default)
                    ->generateUrl()
            );
            return $default . ' ' . $createLink;
        }
    
        return $default;
    }
    
    public function renderLinkHref(string $slug, string $defaultHref, bool $adminLink = true): string
    {
        $content = $this->contentRepo->findOneByDescription($slug);
    
        if ($content && !empty($content->getContentText())) {
            $href = $content->getContentText();
    
            $editLink = '';
            if ($adminLink && $this->security->isGranted('ROLE_ADMIN')) {
                $editLink = sprintf(
                    '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\ContentCrudController')
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($content->getId())
                        ->generateUrl()
                );
            }
    
            return htmlspecialchars($href, ENT_QUOTES) . ' ' . $editLink;
        }
    
        if ($adminLink && $this->security->isGranted('ROLE_ADMIN')) {
            $createLink = sprintf(
                '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/plus-icon.png" alt="edit"></i></a>',
                $this->adminUrlGenerator
                    ->setController('App\\Controller\\Admin\\ContentCrudController')
                    ->setDashboard(DashboardController::class)
                    ->setAction('new')
                    ->set('slug', $slug)
                    ->set('contentText', $defaultHref)
                    ->generateUrl()
            );
            return htmlspecialchars($defaultHref, ENT_QUOTES) . ' ' . $createLink;
        }
    
        return htmlspecialchars($defaultHref, ENT_QUOTES);
    }
    
    public function renderImage(string $slug, string $defaultSrc, string $alt = '', string $classes = ""): string
    {
        $image = $this->imageRepo->findOneBy(['slug' => $slug]);
    
        $imageTag = sprintf(
            '<img class="%s" src="%s" alt="%s" />',
            $classes ? htmlspecialchars($classes) : '',
            $image ? htmlspecialchars('/site/web_images/' . $image->getSrc(), ENT_QUOTES) : htmlspecialchars($defaultSrc, ENT_QUOTES),
            htmlspecialchars($alt ?: ($image ? $image->getTitle() : ''), ENT_QUOTES)
        );
    
        $editLink = '';
        if ($this->security->isGranted('ROLE_ADMIN')) {
            if ($image) {
                // Edit link if the image is found
                $editLink = sprintf(
                    '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/edit-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\ImageCrudController')
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($image->getId())
                        ->generateUrl()
                );
            } else {
                // Create link if no image found
                $editLink = sprintf(
                    '<a onclick="openModal(event, this)" href="%s"><i><img style="height:20px" src="/site/images/plus-icon.png" alt="edit"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\ImageCrudController')
                        ->setDashboard(DashboardController::class)
                        ->setAction('new')
                        ->set('slug', $slug) // Pre-fill the slug field
                        ->generateUrl()
                );
            }
        }
    
        return $imageTag . ' ' . $editLink;
    }
        public function renderBackgroundImage(string $slug, string $defaultSrc): string
    {
        $image = $this->imageRepo->findOneBy(['slug' => $slug]);

        // If the image is found, use its path from the database; otherwise, use the defaultSrc
        $imageUrl = $image ? htmlspecialchars('/site/web_images/' . $image->getSrc(), ENT_QUOTES) : htmlspecialchars($defaultSrc, ENT_QUOTES);

        // Return the background-image CSS
        return sprintf("background-image: url('%s');", $imageUrl);
    }
}
