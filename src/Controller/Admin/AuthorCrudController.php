<?php

namespace App\Controller\Admin;

use App\Entity\Author;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AuthorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Author::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Author');
        yield TextField::new("name")->setLabel("Author's Name");
        yield ImageField::new("photo")->setLabel("Photo")
            ->setUploadDir("/public/site/images/profile_pictures")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/profile_pictures");
        yield TextareaField::new("biography")->setLabel("Biography")
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_content.html.twig')
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

            

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
