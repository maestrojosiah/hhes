<?php

namespace App\Controller\Admin;

use App\Entity\Preference;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PreferenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Preference::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("user")->setLabel("User")
            ->autocomplete();
        yield TextField::new("defaultShippingAddress")->setLabel("Default Address");
        yield ChoiceField::new("language")
            ->setLabel("Language")
            ->setChoices([
                'English' => 'english',
            ]);
        yield ChoiceField::new("notificationSettings")
            ->setLabel("Notification")
            ->setChoices([
                "Text Messages"=> "text",
                "Email Message"=> "email",
            ])
            ->hideOnIndex();
        yield ChoiceField::new("communicationPref")
            ->setLabel("Communication Preference")
            ->setChoices([
                "Monthly"=> "monthly",
                "Quarterly"=> "quarterly",
            ])
        ->hideOnIndex();
        yield ChoiceField::new("privacySettings")
            ->setLabel("Privacy Settings")
            ->setChoices([
                "Public"=> "public",
                "Private"=> "private",
            ]);
        yield ChoiceField::new("emailPreference")
            ->setLabel("Email Preference")
            ->setChoices([
                "Send Emails"=> "on",
                "Do Not Send Emails"=> "off",
            ]);
        yield ChoiceField::new("marketingConsent")
            ->setLabel("Marketing Consent")
            ->setChoices([
                "Receive Marketing Emails"=> "on",
                "Do not receive Marketing emails"=> "off",
            ])
;
    }

}
