<?php

namespace App\Controller\Admin;

use App\Entity\BlogCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class BlogCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BlogCategory::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Blog Category Details');
        yield TextField::new("categoryName")->setLabel("Category Name");
        yield TextField::new("slug")->setLabel("Slug");
        yield TextareaField::new("description")->setLabel("Description")
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_content.html.twig')
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
            ;
        yield ImageField::new("imageUrl")->setLabel("Image")
            ->setUploadDir("/public/site/images/uploads")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/uploads")
            ->hideOnIndex();
        yield BooleanField::new("isActive")->setLabel("Is Active?")
            ->renderAsSwitch(false);
        yield IntegerField::new("displayOrder")->setLabel("Display Order")
            ->hideOnIndex();

    }

    
    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
            ->disable(Action::DETAIL);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Blog Category")
            ->setEntityLabelInPlural("Blog Categories")
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);

    }


}
