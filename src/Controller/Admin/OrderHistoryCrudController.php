<?php

namespace App\Controller\Admin;

use App\Entity\OrderHistory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderHistoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderHistory::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("user")->setLabel("User")
            ->autocomplete();
        // yield AssociationField::new("shippingAddress")->setLabel("Shipping Address");
        yield TextField::new("orderStatus")->setLabel("Order Status");
        yield TextField::new("paymentMethod")->setLabel("Payment");
        yield IntegerField::new("totalAmount")->setLabel("Total");
        yield IntegerField::new("taxAmount")->setLabel("Tax");
        yield IntegerField::new("shippingAmount")->setLabel("Shipping");
        yield IntegerField::new("subtotal")->setLabel("Subtotal");
        yield IntegerField::new("orderDiscount")->setLabel("Discount");
        yield TextField::new("trackingNumber")->setLabel("Tracking No.");
        yield TextField::new("shippingMethod")->setLabel("Shipping Method");
        yield TextField::new("paymentStatus")->setLabel("Payment Status");
        yield DateField::new("dateOfPayment")->setLabel("Paid On");
        yield TextareaField::new("notes")->setLabel("Notes");
        yield TextField::new("promoCode")->setLabel("Promo");
        // yield TextField::new("deliveryStatus")->setLabel("Delivery Status");
        yield DateField::new("deliveryDate")->setLabel("Delivery Date");
        yield DateField::new("orderDate")->setLabel("Order Date");
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['order_date' => 'DESC']
            )
            ->setEntityLabelInSingular("Order History")
            ->setEntityLabelInPlural("Order Histories");

    }
    

}
