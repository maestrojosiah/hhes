<?php

namespace App\Controller\Admin;

use App\Entity\Book;
use App\Service\CsvExporter;
use App\Service\WordExporter;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BookCrudController extends AbstractCrudController
{

    public function __construct(private AdminUrlGenerator $adminUrlGenerator){}
    
    public static function getEntityFqcn(): string
    {
        return Book::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $entity = new Book();
        $entity->setUser($this->getUser());
        $entity->setUploaded(new \DateTime());

        return $entity;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6);
        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("slug")->setLabel("Slug")
            ->setHelp('Small letters, with hyphens instead of spaces')
            ->setPermission('ROLE_CONTRIBUTOR');
        yield ChoiceField::new("format")->setLabel("Book Format")
            ->setChoices([
                'Hardcover' => 'hardcover',
                'Paperback' => 'paperback'
            ])
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;
        yield TextField::new("isbn")->setLabel("ISBN")
        ->hideOnIndex()
        ->setPermission('ROLE_CONTRIBUTOR');
        ;
        yield ImageField::new("image")->setLabel("Book Image")
            ->setUploadDir("/public/site/images/books")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/books");
            // ->hideOnIndex();
            // ->setHelp("800 x 394 pixels");
        yield AssociationField::new("publisher")->setLabel("Publisher")
            ->hideOnIndex();
        yield AssociationField::new("presentations")->setLabel("Presentations")
            ->onlyOnDetail()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;
        yield BooleanField::new("deleted")->setLabel("Deleted?")
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;
        yield BooleanField::new("banner")->setLabel("Use As Banner?")
            ->setPermission('ROLE_CONTRIBUTOR');
        ;
        yield BooleanField::new("featured")->setLabel("Is Featured?")
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;
        yield AssociationField::new("author")->setLabel("Author(s)")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
            ->hideOnIndex();


        yield FormField::addColumn(6);
        yield TextareaField::new("description")->setLabel("Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

        yield AssociationField::new("category")->setLabel("Categories")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;
        yield IntegerField::new("cost")->setLabel("Cost");
        yield ChoiceField::new("language")->setLabel("Language")
            ->setChoices([
                'English' => 'english',
                'Swahili' => 'swahili',
            ])
            ->hideOnIndex();
        yield ChoiceField::new("availability")->setLabel("Availability")
            ->setChoices([
                'Available' => 'available',
                'Not Available' => 'not_available',
            ])
            ->hideOnIndex();
        yield DateField::new("uploaded")->setLabel("Uploaded on")
            ->hideOnForm()
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
            ;        

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }

    // add a button on top 'Add a note to your invoice'
    public function configureActions(Actions $actions): Actions
    {
        $exportAction = Action::new('export', 'Export CSV', 'fa fa-download')
            ->addCssClass('btn btn-info btn-sm')
            ->linkToCrudAction('export')
            ->createAsGlobalAction();

        $exportWord = Action::new('exportWord', 'Export to Word', 'fa fa-file-word')
            ->addCssClass('btn btn-primary btn-sm')
            ->linkToCrudAction('exportWord')
            ->createAsGlobalAction();

        // $notesUrl = $this->adminUrlGenerator
        //     ->setRoute("app_index", ['e-n' => $this->getContext()->getEntity()->getInstance()->getId()])
        //     ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $preview = Action::new('add_presentation', 'Add Presentation', 'fa fa-add')
            ->addCssClass('btn btn-info btn-sm')
            ->linkToUrl(function ($entity) {
                // Assuming getId() is the method to get the ID of your entity
                $entityId = $entity->getId();
    
                if ($entityId !== null) {
                    $url = $this->adminUrlGenerator
                    ->setController(PresentationCrudController::class)
                    ->setAction('new')
                    ->set('book', $entityId)
                    ->generateUrl();
                } else {
                    $url = "#";
                }
    
                // Handle the case where the entity ID is null
                return $url; // or any other fallback URL
            });
    
        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $preview)
        ->add(Crud::PAGE_INDEX, $exportAction)
        ->add(Crud::PAGE_INDEX, $exportWord)
        ->setPermission($preview, 'ROLE_CONTRIBUTOR')        
        ;
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }

    public function export(AdminContext $context, CsvExporter $csvExporter)
    {
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $context->getCrud()->setFieldAssets($this->getFieldAssets($fields));
        $filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $queryBuilder = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters);

        // $user = $context->getUser();
        // if(!$user instanceof User){
        //     throw new \LogicException('User not instance of User.');
        // }
        // $fullname = $user->getFullname();
        return $csvExporter->createResponseFromQueryBuilder(
            $queryBuilder,
            $fields,
            'hhes_books.csv'
        );
    }

    public function exportWord(AdminContext $context, WordExporter $wordExporter)
    {
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $context->getCrud()->setFieldAssets($this->getFieldAssets($fields));
        $filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $queryBuilder = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters);

        return $wordExporter->createResponseFromQueryBuilder(
            $queryBuilder,
            $fields,
            'hhes_books.docx'
        );
    }



}
