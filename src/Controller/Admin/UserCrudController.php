<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Service\CsvExporter;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class UserCrudController extends AbstractCrudController
{

    public function __construct(private UserPasswordHasherInterface $passwordHasher, private LoggerInterface $logger, private AdminUrlGenerator $adminUrlGenerator, private MailerInterface $mailer)
    {}    

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->updateConstituencies($entityInstance);
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function createEntity(string $entityFqcn)
    {
        $user = new User();
        $user->setRegistrationDate(new \DateTime());

        $phoneNumber = $user->getPhone();
        if ($phoneNumber) {
            $hashedPassword = $this->passwordHasher->hashPassword($user, $phoneNumber);
            $user->setPassword($hashedPassword);
        }

        return $user;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setRegistrationDate(new \DateTime());
        $this->updateConstituencies($entityInstance);
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    private function updateConstituencies(User $user): void
    {
        $user->getConstituency()->clear();
        foreach ($user->getCounty() as $county) {
            foreach ($county->getConstituencies() as $constituency) {
                $user->addConstituency($constituency);
            }
        }
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if($this->isGranted('ROLE_SUPER_ADMIN')){
            return $queryBuilder;
        }

        $user = $this->getUser();

        if(!$user instanceof User){
            throw new \LogicException('Not user');
        }

        return $queryBuilder
            ->andWhere('entity.id = :id')
            ->setParameter('id', $user->getId());

        // return $queryBuilder;
    }

    #[IsGranted("ROLE_ADMIN")]
    public function configureFields(string $pageName): iterable
    {
        $user = $this->getUser();
        if(!$user instanceof User) {
            throw new \LogicException('Not user');
        }

        $urlToCounties = $this->adminUrlGenerator
            ->unset('entityId') // Reset the URL generator to clear any previous state
            ->setController(CountyCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        yield FormField::addTab("Basic Info")
            ->setHelp('Saving a user from here will use their phone number as their initial password.');
        yield FormField::addColumn(6);
        if($user->getUsertype() == 'customer'){
            // do nothing
        } else {
            yield TextField::new("username")->setLabel("Username")
            ->setHelp('With no space, all small letters')
            ->hideOnIndex();
        }
        yield TextField::new("fullName")->setLabel("Full Name")
            ->hideOnForm();
        yield TextField::new("fName")->setLabel("First Name")
            ->onlyOnForms();
        yield TextField::new("lName")->setLabel("Last Name")
            ->onlyOnForms();
        yield EmailField::new("email")->setLabel("Email");
        yield ChoiceField::new("usertype")->setLabel("Usertype")
            ->setChoices([
                'User' => 'user',
                'Customer' => 'customer',
                'Admin' => 'admin',
            ])
            ->setPermission('ROLE_CONTRIBUTOR');
;
        yield TextField::new("phone")->setLabel("Phone Number");
        yield ChoiceField::new("gender")->setLabel("Gender")
            ->setChoices([
                'Male' => 'male',
                'Female' => 'female',
            ])
            ->hideOnIndex();

        yield FormField::addColumn(6);
        yield ImageField::new("profilePicture")->setLabel("Photo")
            ->setUploadDir("/public/site/images/profile_pictures")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/profile_pictures")
            ->hideOnIndex();
        yield ChoiceField::new("category")->setLabel("Category")
            ->setChoices([
                'Office' => 'office',
                'ABC' => 'abc',
                'Literature Evangelist' => 'le',
                'Other' => 'other',
            ])
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');

        yield BooleanField::new("is_active")->setLabel("Is Active?")
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
        yield BooleanField::new("profileComplete")->setLabel("Profile Complete?")
            ->onlyOnDetail();
        yield DateField::new("registrationDate")->setLabel("Registration Date")
            ->onlyOnDetail();        
        $roles = ['ROLE_ADMIN', 'ROLE_CUSTOMER', 'ROLE_CONTRIBUTOR', 'ROLE_MODERATOR', 'ROLE_SUPER_ADMIN'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderAsBadges()
            ->renderExpanded()
            ->hideOnIndex()
            ->setPermission('ROLE_CONTRIBUTOR');
        yield TextField::new("residence")->setLabel("Residence");
        if($user->getUsertype() == 'customer'){
            // do nothing
        } else {
            yield AssociationField::new("county")->setLabel("County")
                ->setFormTypeOptions([
                    'by_reference' => false,
                ])
                ->autocomplete()
                ->hideOnIndex();
            yield AssociationField::new("constituency")->setLabel("Constituencies")
                ->setFormTypeOptions([
                    'by_reference' => false,
                ])
                ->autocomplete()
                ->hideOnIndex()
                ->setHelp("To see a full list of constituencies for your county, visit <a class='btn btn-info btn-sm mt-3' target='_blank' href='" .$urlToCounties . "'>Counties</a> page");
        }

        yield FormField::addTab("About");
        yield TextareaField::new("about")->setLabel("About You")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ])            
            ;


    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ])
            // ->setEntityPermission('ADMIN_USER_EDIT')
            ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $exportAction = Action::new('export', 'Export CSV', 'fa fa-download')
            ->addCssClass('btn btn-info btn-sm')
            ->linkToCrudAction('export')
            ->createAsGlobalAction();

            return parent::configureActions($actions)
            ->setPermission(Action::INDEX, 'ROLE_ADMIN')
            ->setPermission($exportAction, 'ROLE_CONTRIBUTOR')
            ->setPermission(Action::EDIT, 'ROLE_ADMIN')
            ->add(Crud::PAGE_INDEX, $exportAction)
            ;
    }

    public function export(AdminContext $context, CsvExporter $csvExporter)
    {
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $context->getCrud()->setFieldAssets($this->getFieldAssets($fields));
        $filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $queryBuilder = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters);

        // $user = $context->getUser();
        // if(!$user instanceof User){
        //     throw new \LogicException('User not instance of User.');
        // }
        // $fullname = $user->getFullname();
        return $csvExporter->createResponseFromQueryBuilder(
            $queryBuilder,
            $fields,
            'users.csv'
        );
    }

}
