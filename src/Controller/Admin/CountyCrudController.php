<?php

namespace App\Controller\Admin;

use App\Entity\County;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CountyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return County::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new("name")->setLabel("County Name");
        yield AssociationField::new("constituencies")->setLabel("Constituencies")
            ->setTemplatePath('admin/fields/constituencies.html.twig')
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
        ;
        yield AssociationField::new("users")->setLabel("Users in County")
            ->setTemplatePath('admin/fields/users.html.twig')    
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
        ;
    }

}
