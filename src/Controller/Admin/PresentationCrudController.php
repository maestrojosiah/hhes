<?php

namespace App\Controller\Admin;

use App\Entity\Presentation;
use App\Repository\BookRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class PresentationCrudController extends AbstractCrudController
{

    public function __construct(private BookRepository $bookRepo, private RequestStack $requestStack, private AdminUrlGenerator $adminUrlGenerator){}

    public static function getEntityFqcn(): string
    {
        return Presentation::class;
    }

    public function createEntity(string $entityFqcn)
    {

        $book = $this->getBook();
        $entity = new Presentation();
        $entity->setUser($this->getUser());
        $entity->setDeleted(0);
        $entity->setBook($book);

        return $entity;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Author');
        yield ChoiceField::new("deleted")->setLabel("Deleted?")
            ->setChoices([
                'No' => 0,
                'Yes' => 1
            ]);
        yield AssociationField::new("book")->setLabel("Book");
        yield ImageField::new("photoPath")->setLabel("Photo")
            ->setUploadDir("/public/img/books")
            ->setUploadedFileNamePattern("img/books/[slug]-[timestamp].[extension]")
            ->setBasePath("");
        yield TextEditorField::new('description')
            ->setLabel('Biography')
            ->hideOnIndex()
            ->setTemplatePath('admin/fields/raw_content.html.twig')
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
            // ->setFormType(CKEditorType::class);
    }

    public function getBook(){
        $request = $this->requestStack->getCurrentRequest();
        $book_id = $request->query->get('book');
        if(null !== $book_id){
            $book = $this->bookRepo->find($book_id);
            return $book;    
        }
        return null;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor.html.twig',
            ]);
    }


    // add a button on top 'Add a note to your invoice'
    public function configureActions(Actions $actions): Actions
    {

        // $notesUrl = $this->adminUrlGenerator
        //     ->setRoute("app_index", ['e-n' => $this->getContext()->getEntity()->getInstance()->getId()])
        //     ->generateUrl();

        $installAssetsAction = Action::new('installAssets', 'Install Assets')
            ->linkToRoute('install_assets');

        // if the method is not defined in a CRUD controller, link to its route
        $preview = Action::new('add_presentation', 'Add', 'fa fa-add')
            ->addCssClass('btn btn-info btn-sm')
            ->linkToUrl(function ($entity) {
                // Assuming getId() is the method to get the ID of your entity
                $entityId = $entity->getBook()->getId();
    
                if ($entityId !== null) {
                    $url = $this->adminUrlGenerator
                    ->setController(PresentationCrudController::class)
                    ->setAction('new')
                    ->set('book', $entityId)
                    ->generateUrl();
                } else {
                    $url = "#";
                }
    
                // Handle the case where the entity ID is null
                return $url; // or any other fallback URL
            });
    
        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $preview)
        ->add(Crud::PAGE_INDEX, $installAssetsAction);
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }

}
