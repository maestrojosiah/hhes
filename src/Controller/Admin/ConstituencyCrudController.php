<?php

namespace App\Controller\Admin;

use App\Entity\Constituency;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ConstituencyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Constituency::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new("name")->setLabel("Constituency Name");
        yield AssociationField::new("county")->setLabel("County");
        yield AssociationField::new("users")->setLabel("Users in Constituency")
            ->setTemplatePath('admin/fields/users.html.twig')    
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
        ;
    }
}
