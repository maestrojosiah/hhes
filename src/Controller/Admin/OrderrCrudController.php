<?php

namespace App\Controller\Admin;

use App\Entity\Orderr;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Registry\TemplateRegistry;

class OrderrCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Orderr::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if($this->isGranted('ROLE_CONTRIBUTOR')){
            return $queryBuilder;
        }

        $user = $this->getUser();

        if(!$user instanceof User){
            throw new \LogicException('Not user');
        }

        if($user->getCategory() == 'le'){
            return $queryBuilder
                ->andWhere('entity.agent = :id')
                ->setParameter('id', $user->getId());
        } else {
            return $queryBuilder
                ->andWhere('entity.user = :id')
                ->setParameter('id', $user->getId());
        }

        // return $queryBuilder;
    }

    public function configureFields(string $pageName): iterable
    {
        yield DateField::new("orderDate")->setLabel("Order Date")
            ->hideWhenCreating();
        yield AssociationField::new("user")->setLabel("User")
            ->autocomplete();
        yield AssociationField::new("agent")->setLabel("Agent")
            ->autocomplete();
        yield AssociationField::new("shippingAddress")->setLabel("Shipping Address")
            ->autocomplete()
            ->hideOnIndex();
        yield AssociationField::new("orderItems")->setLabel("Order Items")
        ->onlyOnDetail()    
        ->setTemplatePath('admin/fields/order_items.html.twig');
        yield ChoiceField::new("orderStatus")
            ->setLabel("Order Status")
            ->setChoices([
                'Pending' => 'pending',
                'Processing' => 'processing',
                'Shipped/Dispatched' => 'shipped',
                'Delivered' => 'delivered',
                'Canceled' => 'canceled',
                'On Hold' => 'on_hold',
                'Backordered' => 'backordered',
                'Returned' => 'returned',
                'Refunded' => 'refunded',
                'Fraudulent' => 'fraudulent',
                'Partially Shipped' => 'partially_shipped'
            ])
            ->hideOnIndex();
        yield ChoiceField::new("paymentMethod")
            ->setLabel("Payment")
            ->setChoices([
                "Mpesa On Delivery"=> "mpesa_on_delivery",
                "Mpesa Online" => "mpesa",
            ])
            ->hideOnIndex();
        yield IntegerField::new("totalAmount")->setLabel("Total")->hideOnIndex();
        yield IntegerField::new("taxAmount")->setLabel("Tax")->hideOnIndex();
        yield IntegerField::new("shippingAmount")->setLabel("Shipping")->hideOnIndex();
        yield IntegerField::new("subtotal")->setLabel("Subtotal");
        yield IntegerField::new("discountAmount")->setLabel("Discount")->hideOnIndex();
        yield TextField::new("trackingNumber")->setLabel("Tracking No.");
        yield ChoiceField::new("shippingMethod")
            ->setLabel("Shipping Method")
            ->setChoices([
                "Pick Up" => "store_pick_up",
                "Delivery" => "delivery"
            ]);
        yield ChoiceField::new("paymentStatus")->setLabel("Payment Status")
            ->setChoices([
                'Pending' => 'pending',
                'Processing' => 'processing',
                'Completed' => 'completed',
                'Failed' => 'failed',
                'Refunded' => 'refunded',
                'Chargeback' => 'chargeback',
                'Cancelled' => 'cancelled',
                'Authorized' => 'authorized',
                'Partially Paid' => 'partially_paid'
            ]);
        yield DateField::new("paymentDate")->setLabel("Paid On")
            ->hideOnIndex();
        yield TextEditorField::new("notes")->setLabel("Notes")
            ->hideOnIndex();
        yield TextField::new("promoCode")->setLabel("Promo")
            ->hideOnIndex();
        yield ChoiceField::new("deliveryStatus")->setLabel("Delivery Status")
            ->setChoices([
                'Not Shipped' => 'not_shipped',
                'In Transit' => 'in_transit',
                'Out for Delivery' => 'out_for_delivery',
                'Delivered' => 'delivered',
                'Failed Delivery Attempt' => 'failed_delivery_attempt',
                'Returned to Sender' => 'returned_to_sender',
                'Pending Pickup' => 'pending_pickup'            
            ])
            ->hideOnIndex();
        yield DateField::new("deliveryDate")->setLabel("Delivery Date")
            ->hideWhenCreating()
            ->hideOnIndex();

    }


    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['orderDate' => 'DESC']
            )
            ->setEntityLabelInSingular("Order")
            ->setEntityLabelInPlural("Orders");

    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->setPermission(Action::DELETE, 'ROLE_CONTRIBUTOR')
            ;
    }

}
// TemplateRegistry::register("", function (TemplateRegistry $templateRegistry) {