<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use App\Entity\Author;
use App\Entity\Blog;
use App\Entity\BlogCategory;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\CommentReplies;
use App\Entity\Constituency;
use App\Entity\Content;
use App\Entity\County;
use App\Entity\FAQ;
use App\Entity\Image;
use App\Entity\Media;
use App\Entity\Message;
use App\Entity\OrderHistory;
use App\Entity\OrderItem;
use App\Entity\Orderr;
use App\Entity\Page;
use App\Entity\Preference;
use App\Entity\Presentation;
use App\Entity\Publisher;
use App\Entity\Section;
use App\Entity\SEO;
use App\Entity\Setting;
use App\Entity\Tag;
use App\Entity\Testimonials;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator){}

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }
        return $this->redirect($this->adminUrlGenerator->setController(UserCrudController::class)->generateUrl());

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        $user = $this->getUser();
        if(!$user instanceof User){
            throw new \LogicException('Not user');
        }
        $usertype = strtoupper($user->getUsertype());
        return Dashboard::new()
            ->setTitle( 'HHES KE ' . $usertype )
            ->setFaviconPath('/site/favicon.ico')
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);

        yield Menuitem::section('People');
        if($this->isGranted('ROLE_CONTRIBUTOR')) {
            yield MenuItem::linkToCrud('Users', 'fas fa-users', User::class);
            yield MenuItem::linkToCrud('Testimonials', 'fas fa-comment', Testimonials::class);
        } else {
            yield MenuItem::linkToCrud('Me', 'fas fa-user', User::class);
        }
        yield MenuItem::linkToCrud('Authors', 'fas fa-users', Author::class)
            ->setPermission('ROLE_MODERATOR');

        yield Menuitem::section('Books');
        yield MenuItem::linkToCrud('Books', 'fas fa-tags', Book::class);
        
        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield MenuItem::linkToCrud('Categories', 'fas fa-tags', Category::class);
            yield MenuItem::linkToCrud('Presentations', 'fas fa-chalkboard', Presentation::class);
            yield MenuItem::linkToCrud('Publishers', 'fas fa-book', Publisher::class);

            yield Menuitem::section('Web Content');
            yield MenuItem::linkToCrud('Text Content', 'fas fa-globe', Content::class);
            yield MenuItem::linkToCrud('Image Content', 'fas fa-image', Image::class);
            yield MenuItem::linkToCrud('Sections', 'fas fa-columns', Section::class);
            yield MenuItem::linkToCrud('Pages', 'fas fa-file', Page::class);
    
            yield Menuitem::section('Settings');
            yield MenuItem::linkToCrud('Basic Settings', 'fas fa-gear', Setting::class);
            yield MenuItem::linkToCrud('SEO Settings', 'fas fa-search', SEO::class);
    
        }
        yield Menuitem::section('Orders');
        yield MenuItem::linkToCrud('Orders', 'fas fa-shopping-cart', Orderr::class);
        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield MenuItem::linkToCrud('Order Items', 'fas fa-list', OrderItem::class);
            yield MenuItem::linkToCrud('Addresses', 'fas fa-map-pin', Address::class);
        }
        // yield MenuItem::linkToCrud('Order History', 'fas fa-history', OrderHistory::class);

        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield Menuitem::section('Blog');
            yield MenuItem::linkToCrud('Blog', 'fas fa-list', Blog::class);
            yield MenuItem::linkToCrud('Blog Categories', 'fas fa-list', BlogCategory::class);
            yield MenuItem::linkToCrud('Blog Comments', 'fas fa-comment', Comment::class);
            yield MenuItem::linkToCrud('Comment Replies', 'fas fa-comment', CommentReplies::class);
            yield MenuItem::linkToCrud('Blog Tags', 'fas fa-tag', Tag::class);
            yield MenuItem::linkToCrud('Blog Media', 'fas fa-camera', Media::class);
        }
        
        yield Menuitem::section('Places');
        yield MenuItem::linkToCrud('Counties', 'fas fa-shopping-cart', County::class);
        yield MenuItem::linkToCrud('Constituencies', 'fas fa-list', Constituency::class);

        yield Menuitem::section('More');
        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield MenuItem::linkToCrud('FAQ', 'fas fa-question-circle', FAQ::class);
            yield MenuItem::linkToCrud('Messages', 'fas fa-envelope', Message::class);
        }
        yield MenuItem::linkToCrud('Settings', 'fas fa-gear', Preference::class);
        yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_index'));

    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-info')->setHtmlAttributes(['title' => 'View'])->setIcon('fa fa-eye')->setLabel("View");
            }) 
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-primary')->setHtmlAttributes(['title' => 'Edit'])->setIcon('fa fa-edit')->setLabel("Edit");
            }) 
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-outline-secondary action-delete')->setHtmlAttributes(['title' => 'Delete'])->setIcon('fa fa-trash')->setLabel("Delete");
            }) 
            ->setPermission(Action::INDEX, 'ROLE_ADMIN')
            ->setPermission(Action::DETAIL, 'ROLE_ADMIN')
            ->setPermission(Action::EDIT, 'ROLE_MODERATOR')
            ->setPermission(Action::NEW, 'ROLE_CONTRIBUTOR')
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')
            ->setPermission(Action::BATCH_DELETE, 'ROLE_SUPER_ADMIN')
            ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        if (!$user instanceof User) {
            throw new \Exception('Wrong user');
        }

        return parent::configureUserMenu($user)
            ->setAvatarUrl("/site/images/profile_pictures/".$user->getAvatarUri())
            ->setMenuItems([
                MenuItem::linkToUrl('My Profile','fas fa-user', $this->generateUrl('app_profile_show')),
                MenuItem::linkToUrl('Logout', 'fa fa-sign-out', $this->generateUrl('app_logout')),
            ]);

    }

    // public function configureAssets(): Assets
    // {
    //     return parent::configureAssets()
    //         ->addCssFile('/site/css/test.css'); 
    // }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->showEntityActionsInlined();
    }    

    public function configureAssets(): Assets
    {
        // return parent::configureAssets()
            // ->addCssFile('styles/app.css')
            // ->addJsFile('app.js')
            // ->addJsFile('vendor/chart.js/auto.js');
            // ->addJsFile('/invoices/js/jquery-1.9.1.js')
            // ->addJsFile('/invoices/js/spectrum.js')
            // ->addJsFile('/invoices/js/configSpectrum.js');
        $assets = parent::configureAssets()
        // ->addCssFile('site/css/bootstrap.css')
        ->addCssFile('site/css/material-dashboard.css')
        ->addCssFile('assets/css/admin.css')
        ;
        // $assets->addAssetMapperEntry('app');
        return $assets;
    }


}
