<?php

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class BrokenLinksController extends AbstractController
{
    #[Route('/broken/links', name: 'app_broken_links')]
    public function index(): Response
    {
        return $this->render('broken_links/index.html.twig', [
            'controller_name' => 'BrokenLinksController',
        ]);
    }

    #[Route('/book/{id}.html', name: 'show_book_redirect', requirements: ['id' => '\d+'])]
    public function redirectToNewUrl(BookRepository $bookRepository, int $id): RedirectResponse
    {
        $book = $bookRepository->find($id);
        if (!$book) {
            throw $this->createNotFoundException('The book does not exist');
        }

        return $this->redirectToRoute('show_book', ['slug' => $book->getSlug()], 301);
    }

}
