<?php

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'search_books')]
    public function search(Request $request, BookRepository $bookRepository): JsonResponse
    {
        $query = $request->query->get('query');
        $categorySlug = $request->query->get('category');

        if($categorySlug == 'all'){
            $books = $bookRepository->searchBooks($query);
        } else {
            $books = $bookRepository->searchBooks($query, $categorySlug);
        }

        $results = [];
        foreach ($books as $book) {
            $results[] = [
                'id' => $book->getId(),
                'title' => $book->getTitle(),
                'slug' => $book->getSlug(),
                'image' => $book->getImage(),
                'cost' => $book->getCost(),
                'description' => $book->getDescription(),
                'categories' => array_map(fn($category) => $category->getName(), $book->getCategory()->toArray()),
                'authors' => array_map(fn($author) => $author->getName(), $book->getAuthor()->toArray()),
                'publisher' => $book->getPublisher() ? $book->getPublisher()->getName() : '',
            ];
        }

        return new JsonResponse($results);
    }
}
