<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use App\Repository\PageRepository;
use App\Service\PageDataService;
use App\Service\SeoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ShopController extends AbstractController
{
    public function __construct(private PageDataService $pageDataService, private PageRepository $pageRepository)
    {}
    #[Route('/shop/{category?}', name: 'app_shop')]
    public function index(Request $request, SeoService $seoService, BookRepository $bookRepo, CategoryRepository $categoryRepo, $category): Response
    {

        $categories = $categoryRepo->findAll();
        $userPref = $request->query->get('pref', 'list'); // Default to 'list'
    
        // Logic to process user preference (optional)
        $shop_page = 'shop?pref=' . $userPref;

        $template = "shop/$userPref.html.twig";
        $page = $request->query->getInt('page', 1);
        $limit = 12;

        // Count all books
        if(null == $category){
            $totalBooks = $bookRepo->createQueryBuilder('b')
                ->select('COUNT(b.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            $totalBooks = $bookRepo->createQueryBuilder('b')
                ->innerJoin('b.category', 'c')
                ->where('c.slug IN (:slugs)')
                ->setParameter('slugs', [$category])
                ->select('COUNT(b.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        

    
        $category = $categoryRepo->findOneBySlug($category);

        if (null !== $category) {
            $query = $bookRepo->findByBookCategoriesQuery([$category]);
        } else {
            $query = $bookRepo->findAllQuery();
        }
    
        $paginator = $this->paginate($query, $page, $limit);

        $pageData = $this->pageDataService->getPageData($request, $shop_page);
        $pageData['books'] = $paginator['data'];
        $pageData['totalPages'] = $paginator['totalPages'];
        $pageData['currentPage'] = $page;
        $pageData['category'] = $category;
        $pageData['categories'] = $categories;
        $pageData['totalItems'] = $totalBooks;
        $pageData['userPref'] = $userPref;
        $pageData['meta_tags'] = $seoService;
        $pageData['limit'] = $limit;
        $pageData['title'] .= " " . $category . " Books";

        $metaTitle = $pageData['page']->getMetaTitle();
        $metaDescription = $pageData['page']->getMetaDescription();
        if(null == $category){
            $fullTitle = $metaTitle;
            $fullDescription = $metaDescription;
        } else {
            $fullTitle = $category->getName() . " " . $metaTitle;
            $fullDescription = "Buy " . $category->getName() . " " . $metaDescription;
        }
        $pageData['page']->setMetaTitle($fullTitle);
        $pageData['page']->setMetaDescription($fullDescription);

        return $this->render('index/page.html.twig', $pageData);

    }

    private function paginate($query, $page, $limit)
    {
        $totalItems = count($query->getResult());
        $totalPages = ceil($totalItems / $limit);
    
        $query->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);
    
        return [
            'data' => $query->getResult(),
            'totalPages' => $totalPages,
        ];
    }    

    
}
