<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\DiscountUsage;
use App\Entity\Message;
use App\Entity\User;
use App\Form\CartType;
use App\Manager\CartManager;
use App\Repository\AddressRepository;
use App\Repository\DiscountCodeRepository;
use App\Repository\DiscountUsageRepository;
use App\Repository\OrderrRepository;
use App\Repository\BookColorRepository;
use App\Repository\CountyRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\SeoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class CartController extends AbstractController
{

    public function __construct(private SeoService $seoService, private EntityManagerInterface $em, private UserRepository $userRepo, private Mailer $mailer, private AdminUrlGenerator $adminUrlGenerator)
    {}

    #[Route('/cart', name: 'cart')]
    public function index(CartManager $cartManager, Request $request): Response
    {
        $cart = $cartManager->getCurrentCart($this->getUser());
        
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cart = $form->getData();
            // echo "<pre> from cart index";
            // var_dump($cart->getId());
            // print_r(count($form->getData()->getOrderItems()) );
            foreach ($form->getData()->getOrderItems() as $item) {
                // echo $item->getQuantity()."<br>";
                $totalPrice = $this->calculateTotalPrice($item->getQuantity(), $item->getPricePerUnit());
                $subtotal = $this->calculateSubtotal($totalPrice, 0);
                $item->setTotalPrice($totalPrice);
                $item->setSubtotal($subtotal);
                // print_r($totalPrice);
                // print_r($subtotal);
                // echo "<br>";
            }
            // die();
            // die();
            $cart->setOrderDate(new \DateTime());
            $cartManager->save($cart);

            return $this->redirectToRoute('cart');
        }

        $this->seoService->setTitle('Shopping Cart | Home Health Education Service')
            ->setDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle('Shopping Cart | Home Health Education Service')
            ->setOgDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setOgImage('https://hheskenya.org/site/images/books/juices-1718548142.png')
            ->setOgUrl('https://hheskenya.org/cart')
            ->setOgType('website');
       
        if ($cart->getOrderItems()->count() < 1) {
            return $this->redirectToRoute('app_index');
        }
    
        return $this->render('cart/index.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }


    #[Route('/update/cart/shipping/cost', name: 'update_order_shipping_cost')]
    public function updateShippingCost(OrderrRepository $orderrRepository, Request $request)
    {

        $shipping_cost = $request->request->get('shipping_cost');
        $order_id = $request->request->get('order_id');
            
        $order = $orderrRepository->find($order_id);

        $order->setShippingAmount($shipping_cost);
        
        $this->em->persist($order);
        $this->em->flush();

        return new JsonResponse(['shipping_cost' => $shipping_cost, 'totalAmount' => $order->getTotalAmount()]);

    }

    #[Route('/checkout', name:'checkout')]
    public function checkout(CartManager $cartManager, CountyRepository $countyRepo): Response
    {
        $cart = $cartManager->getCurrentCart($this->getUser());


        if ($cart->getOrderItems()->count() < 1) {
            return $this->redirectToRoute('app_index');
        }

        $this->denyAccessUnlessGranted('ROLE_USER');
        $counties = $countyRepo->findAll();

        if($this->getUser()->getUsertype() == 'admin'){
            return $this->redirectToRoute('admin');
        }
        $cart = $cartManager->getCurrentCart($this->getUser());

        $this->seoService->setTitle('Checkout | Home Health Education Service')
            ->setDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle('Checkout | Home Health Education Service')
            ->setOgDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setOgImage('https://hheskenya.org/site/images/books/juices-1718548142.png')
            ->setOgUrl('https://hheskenya.org/checkout')
            ->setOgType('website');
            
        $countyData = $this->getCountyData($counties);

        return $this->render('cart/checkout.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'cart'=> $cart,
            'counties'=> $counties,
            'countyData'=> $countyData,
            'user' => $this->getUser(),
        ]);

    }

    public function getCountyData($counties){
        $countyData = [];
    
        foreach ($counties as $county) {
            $countyName = $county->getName();
            $constituencies = [];
    
            foreach ($county->getConstituencies() as $constituency) {
                $constituencies[] = $constituency->getName();
            }
    
            $countyData[$countyName] = $constituencies;
        }
    
        return $countyData;
    }

    #[Route('/order/received/successfully/{order_id}/{shipping}', name:'order_received')]
    public function orderReceived(CartManager $cartManager, OrderrRepository $orderRepo, Request $request, $order_id, $shipping): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = $orderRepo->findOneById($order_id);

        $this->seoService->setTitle('Order Received | Home Health Education Service')
            ->setDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle('Order Received | Home Health Education Service')
            ->setOgDescription('Discover health and educational books to improve family well-being. HHES Kenya offers resources on health, family, and spiritual growth in Swahili and English, with convenient home or office delivery.')
            ->setOgImage('https://hheskenya.org/site/images/books/juices-1718548142.png')
            ->setOgUrl('https://hheskenya.org/order/received/successfully/'.$cart->getId().'/'.$shipping)
            ->setOgType('website');
            
        return $this->render('cart/order_received.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'cart'=> $cart,
            'shipping' => $shipping,
            'user' => $this->getUser(),
        ]);

    }

    public function calculateTotalPrice($quantity, $price_per_unit, $tax_amount = null): ?float
    {
        if ($quantity === null || $price_per_unit === null) {
            return null;
        }

        $subtotal = $quantity * $price_per_unit;

        // Include tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal += $tax_amount;
        }

        return $subtotal;
    }

    public function calculateSubtotal($total_price, $discount_amount, $tax_amount = null): ?float
    {
        if ($total_price === null || $discount_amount === null) {
            return null;
        }

        $subtotal = $total_price - $discount_amount;

        // Exclude tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal -= $tax_amount;
        }

        return $subtotal;
    }

    #[Route('/submit/checkout', name:'submit_checkout')]
    public function submitCheckout(CartManager $cartManager, Request $request, AddressRepository $addressRepo, OrderrRepository $orderRepo): Response
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
            if (isset($_POST['chosenAgentId'])) {

                $this->denyAccessUnlessGranted('ROLE_USER');
                $date = new \DateTime();
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $streetAddress = $_POST['streetAddress'];
                $shipping_cost = $_POST['shipping'];
                $cartAndShipping = $_POST['cartAndShipping'];
                $county = $_POST['county'];
                $constituency = $_POST['constituency'];
                $phoneNumber = $_POST['phoneNumber'];
                $fullname = $firstName . ' ' . $lastName;
                $notes = $_POST['notes'];
                $chosenAgentId = $_POST['chosenAgentId'];

                // get customer and selected agent
                $user = $this->getUser();
                $agent = $this->userRepo->find($chosenAgentId);

                $existingAddress = $addressRepo->findOneByUser($user);
                
                if($existingAddress === null) {
                    $address = new Address();
                } else {
                    $address = $existingAddress;
                }
                
                // save address
                $address->setUser($user);
                $address->setStreetAddress($streetAddress);
                $address->setCounty($county);
                $this->em->persist($address);
                $this->em->flush();

                // message to send to agent and admin
                $msg = "There is a new order: Name: $fullname, Street Address: $streetAddress, County: $county, Constituency: $constituency, Phone Number: $phoneNumber ";

                // get the cart associated with this user
                $cart = $cartManager->getCurrentCart($this->getUser());
                $order = $orderRepo->findOneById($cart->getId());
                $order->setUser($user);
                
                // echo "<pre>";
                // var_dump($cart);
                // die();
                $cart->setOrderStatus('processing');
                $cart->setTotalAmount($cartAndShipping);
                $cart->setTaxAmount(0);
                $cart->setShippingAmount($shipping_cost);
                $cart->setSubtotal($cart->getTotal());
                $cart->setShippingMethod('delivery');
                $cart->setPaymentStatus('pending');
                $cart->setAgent($agent); // save agent
                $cart->setNotes($notes); // save notes
                $cart->setDeliveryStatus('in_transit');
                $cart->setShippingAddress($address);
                
                // create new message in system
                $message = new Message();
                $message->setName($fullname);
                $message->setEmail($user->getEmail());
                $message->setMessage($msg);
                $message->setReceivedOn($date);

                // save message
                $this->em->persist($message);
                $this->em->persist($order);
                $this->em->flush();
                
                // send emails
                $subject = "New Order From Website";
                $intro = "There is a new order from the website.";
                $admins = $this->userRepo->findByUsertype('admin');
                $urlToOrderDetail = $this->adminUrlGenerator->setController('App\Controller\Admin\OrderrCrudController')
                ->setAction('detail')
                ->setEntityId($cart->getId())
                ->generateUrl();
                $maildata = ['name' => $fullname, 'emailAd' => $user->getEmail(), 'subject' => $subject, 'shipping_cost' => $shipping_cost, 'cartAndShipping' => $cartAndShipping, 'message' => $msg, 'order' => $order, 'urlToOrderDetail' => $urlToOrderDetail, 'intro' => $intro];
                
                foreach ($admins as $admin) {
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "orderform.html.twig");
                }
                $this->mailer->sendEmailMessage($maildata, $user->getEmail(), "Your order has been received", "order_received.html.twig");
                $this->mailer->sendEmailMessage($maildata, $agent->getEmail(), $subject, "orderform.html.twig");
        
                $this->addFlash('success','Your Order has been received. ' . $agent->getFullname() . ' will call you soon');
        
                return $this->redirectToRoute('order_received', ['order_id' => $order->getId(), 'shipping' => $shipping_cost]);
                        
                // pay on delivery was clicked
                // save all required data
                // send email to admins and client
                // got to confirmation page and show necessary info
            } else {
                var_dump("No Volunteer Community Educator selected");
                die();
            }
        }
    
        return $this->render('payment_page.html.twig', [
            'test'=> $request->get('test'),
        ]);
    }

}
