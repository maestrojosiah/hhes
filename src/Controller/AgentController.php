<?php
// src/Controller/AgentController.php

namespace App\Controller;

use App\Repository\ConstituencyRepository;
use App\Repository\CountyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class AgentController extends AbstractController
{
    public function __construct(private UserRepository $userRepository){}

    #[Route('/vce/{username}', name: 'set_agent')]
    public function setAgent(string $username, SessionInterface $session, UserRepository $userRepo): RedirectResponse
    {
        $user = $userRepo->findOneByUsername($username);
        if( null !== $user){

            // Store the agent's username in the session
            $session->set('agent_username', $username);

            // Also store the agent's username in a cookie that lasts for 1 year
            $response = $this->redirectToRoute('app_index');
            $response->headers->setCookie(new Cookie('agent_username', $username, strtotime('+1 year')));

        } else {
            
            $response = $this->redirectToRoute('app_index');

        }


        return $response;
    }
    
    #[Route('/fetch-agents', name: 'fetch_agents', methods: ['POST'])]
    public function fetchAgents(
        Request $request,
        CountyRepository $countyRepo,
        ConstituencyRepository $constituencyRepo,
        UserRepository $userRepository,
        CsrfTokenManagerInterface $csrfTokenManager,
        SessionInterface $session
    ): Response {
        $data = $request->request->all();
    
        // Validate CSRF token
        $csrfToken = $data['_csrf_token'] ?? '';
        if (!$csrfTokenManager->isTokenValid(new CsrfToken('fetch_agents', $csrfToken))) {
            return new Response('Invalid CSRF token', Response::HTTP_FORBIDDEN);
        }
    
        $county = $data['county'] ?? '';
        $constituency = $data['constituency'] ?? '';
    
        $countyEntity = $countyRepo->findOneByName($county);
        $constituencyEntity = $constituencyRepo->findOneByName($constituency);
    
        // Fetch agents based on the criteria
        $agents = $userRepository->findAgents($countyEntity, $constituencyEntity);
        $allAgents = $userRepository->findAgents($countyEntity, $constituencyEntity);
    
        $firstAgent = count($agents) > 0 ? array_shift($agents) : null;
        $otherAgents = $agents;
    
        // Check if an agent is stored in the session
        $agentUsername = $session->get('agent_username');
    
        // If the session does not have the agent username, check the cookie
        if (!$agentUsername) {
            $agentUsername = $request->cookies->get('agent_username');
        }
    
        $selectedAgent = null;
        $htmlSelectedAgent = null;
        if ($agentUsername) {
            $selectedAgent = $this->userRepository->findOneByUsername($agentUsername);
            $htmlSelectedAgent = $this->renderView('agent/_agent_card.html.twig', ['agent' => $selectedAgent]);
        }
    
        // Render the agents to a Twig template
        $htmlFirstAgent = $this->renderView('agent/_agent_card.html.twig', ['agent' => $firstAgent]);
        $htmlOtherAgents = $this->renderView('agent/_agent_cards.html.twig', ['agents' => $otherAgents]);
        $htmlAllAgents = $this->renderView('agent/_agent_cards.html.twig', ['agents' => $allAgents, 'action' => 'view_profile']);
    
        return $this->json([
            'htmlSelectedAgent' => $htmlSelectedAgent ? $htmlSelectedAgent : $htmlFirstAgent,
            'htmlOtherAgents' => $htmlOtherAgents,
            'htmlAllAgents' => $htmlAllAgents,
            'selectedAgentId' => $selectedAgent ? $selectedAgent->getId() : ($firstAgent ? $firstAgent->getId() : null),
            'agentUsername' => $agentUsername
        ]);
    }

    
}
