<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\CommentReplies;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepliesRepository;
use App\Repository\CommentRepository;
use App\Repository\TagRepository;
use App\Service\SeoService;
use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BlogController extends AbstractController
{

    public function __construct(private SeoService $seoService, private BlogCategoryRepository $blogCategoryRepository, private EntityManagerInterface $em){}

    #[Route('/blog/post/{blog_slug}', name: 'blog_show')]
    public function show(TagRepository $tagRepo, BlogRepository $blogRepository, BlogCategoryRepository $categoryRepo, $blog_slug): Response
    {
        $blogPost = $blogRepository->findOneBySlug($blog_slug);
        $blogCategories = $this->blogCategoryRepository->findAll();
        $blogPosts = $blogRepository->findBy(
            ['is_featured'=> 1],
            ['id' => 'ASC'],
            4
        );
        $tags = $tagRepo->findAll();
        $latestBlogPosts = $blogRepository->findBy(
            [],
            ['id'=> 'DESC'],
            2,
        );

        $categoriesWithCounts = $categoryRepo->findCategoriesWithBlogCounts();
        $commentsArray = $blogPost->getComments()->toArray();

        $this->seoService->setTitle($blogPost->getTitle())
            ->setDescription(substr($blogPost->getMetaDescription(), 0, 165))
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle($blogPost->getTitle())
            ->setOgDescription(substr($blogPost->getMetaDescription(), 0, 165))
            ->setOgImage('https://hheskenya.org/site/images/books/juices-1718548142.png')
            ->setOgUrl('https://hheskenya.org/')
            ->setOgType('website');

        return $this->render('blog/show.html.twig', [
            'blogPost' => $blogPost,
            'blogPosts' => $blogPosts,
            'blogCategories' => $blogCategories,
            'tags' => $tags,
            'categoriesWithCounts' => $categoriesWithCounts,
            'latestBlogPosts' => $latestBlogPosts,
            'commentsArray' => $commentsArray,
            'meta_tags' => $this->seoService->generateMetaTags(),
        ]);
    }

    #[Route('/blog/{category}', name: 'app_blog')]
    public function index(CategoryRepository $categoryRepo, BlogRepository $blogRepository, Request $request, $category = null): Response
    {
        $this->seoService->setTitle('Practical life guides - HHES Kenya')
            ->setDescription('Find articles inspired by the books we sell. Grow in all aspects - mentally, physically, spiritually and socially from our all-round articles.')
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle('Practical life guides - HHES Kenya')
            ->setOgDescription('Find articles inspired by the books we sell. Grow in all aspects - mentally, physically, spiritually and socially from our all-round articles.')
            ->setOgImage('https://hheskenya.org/site/images/books/juices-1718548142.png')
            ->setOgUrl('https://hheskenya.org/')
            ->setOgType('website');
    
        $page = $request->query->getInt('page', 1);
        $limit = 10;
    
        $category = $categoryRepo->findOneBySlug($category);
        if (null !== $category) {
            $query = $blogRepository->findByBlogCategoriesQuery([$category]);
        } else {
            $query = $blogRepository->findAllQuery();
        }
    
        $paginator = $this->paginate($query, $page, $limit);
    
        return $this->render('blog/index.html.twig', [
            'blogPosts' => $paginator['data'],
            'totalPages' => $paginator['totalPages'],
            'currentPage' => $page,
            'meta_tags' => $this->seoService->generateMetaTags(),
        ]);
    }
    
    private function paginate($query, $page, $limit)
    {
        $totalItems = count($query->getResult());
        $totalPages = ceil($totalItems / $limit);
    
        $query->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);
    
        return [
            'data' => $query->getResult(),
            'totalPages' => $totalPages,
        ];
    }    
    
    #[Route('/submit/blog/comment', name:'post_comment')]
    public function postComment(BlogRepository $blogRepository, Request $request): Response
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $blog_id = $_POST['blog_id'];
            $content = $_POST['comment'];
            $user = $this->getUser();
            $date = new \DateTime();

            $blog = $blogRepository->find($blog_id);
            
            $comment = new Comment();
            $comment->setBlog($blog);
            $comment->setUser($user);
            $comment->setContent($content);
            $comment->setCommentDate($date);
            $this->em->persist($comment);
            $this->em->flush();

            
        }
    
        return $this->redirectToRoute('blog_show', ['blog_slug' => $blog->getSlug()]);

    }

    #[Route('/submit/comment/reply', name:'comment_reply')]
    public function commentReply(CommentRepository $commentRepository, Request $request): Response
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
            $comment_id = $_POST['comment_id'];
            $content = $_POST['reply'];
            $user = $this->getUser();
            $date = new \DateTime();

            $comment = $commentRepository->find($comment_id);
            $blog = $comment->getBlog();
            
            $reply = new CommentReplies();
            $reply->setComment($comment);
            $reply->setUser($user);
            $reply->setContent($content);
            $reply->setRepliedOn($date);
            $this->em->persist($reply);
            $this->em->flush();

        }
    
        return $this->redirectToRoute('blog_show', ['blog_slug' => $blog->getSlug()]);

    }


    #[Route('/edit/blog/comment/{comment_id}', name: 'edit_comment')]
    public function editComment(CommentRepository $commentRepository, Request $request, EntityManagerInterface $entityManager, int $comment_id): Response
    {
        $comment = $commentRepository->find($comment_id);
    
        // Check if the comment exists
        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }
    
        // Check if the user is the author of the comment or has the necessary permissions to edit
        if ($comment->getUser() !== $this->getUser()) {
            throw new AccessDeniedException('You do not have permission to edit this comment');
        }
    
        if ($request->isMethod('POST')) {
            $content = $request->request->get('comment');
    
            // Validate and update comment content
            // Add your validation logic here if needed
            $comment->setContent($content);
    
            $entityManager->flush();
    
            return $this->redirectToRoute('blog_show', ['blog_slug' => $comment->getBlog()->getSlug()]);
        }
    
        return $this->render('comment/edit.html.twig', ['comment' => $comment]);
    }

    #[Route('/delete/blog/comment/{comment_id}', name: 'delete_comment')]
    public function deleteComment(CommentRepository $commentRepository, EntityManagerInterface $entityManager, int $comment_id): Response
    {
        $comment = $commentRepository->find($comment_id);
    
        // Check if the comment exists
        if (!$comment) {
            throw $this->createNotFoundException('Comment not found');
        }
    
        // Check if the user is the author of the comment or has the necessary permissions to delete
        if ($comment->getUser() !== $this->getUser()) {
            throw new AccessDeniedException('You do not have permission to delete this comment');
        }
    
        // Remove the comment
        $entityManager->remove($comment);
        $entityManager->flush();
    
        return $this->redirectToRoute('blog_show', ['blog_slug' => $comment->getBlog()->getSlug()]);
    }


    #[Route('/edit/blog/reply/{reply_id}', name: 'edit_reply')]
    public function editReply(CommentRepliesRepository $replyRepository, Request $request, EntityManagerInterface $entityManager, int $reply_id): Response
    {
        $reply = $replyRepository->find($reply_id);
    
        // Check if the reply exists
        if (!$reply) {
            throw $this->createNotFoundException('Reply not found');
        }
    
        // Check if the user is the author of the reply or has the necessary permissions to edit
        if ($reply->getUser() !== $this->getUser()) {
            throw new AccessDeniedException('You do not have permission to edit this reply');
        }
    
        if ($request->isMethod('POST')) {
            $content = $request->request->get('reply');
    
            // Validate and update reply content
            // Add your validation logic here if needed
            $reply->setContent($content);
    
            $entityManager->flush();
    
            return $this->redirectToRoute('blog_show', ['blog_slug' => $reply->getComment()->getBlog()->getSlug()]);
        }
    
        return $this->render('reply/edit.html.twig', ['reply' => $reply]);
    }

    #[Route('/delete/blog/reply/{reply_id}', name: 'delete_reply')]
    public function deleteReply(CommentRepliesRepository $replyRepository, EntityManagerInterface $entityManager, int $reply_id): Response
    {
        $reply = $replyRepository->find($reply_id);
    
        // Check if the reply exists
        if (!$reply) {
            throw $this->createNotFoundException('Reply not found');
        }
    
        // Check if the user is the author of the reply or has the necessary permissions to delete
        if ($reply->getUser() !== $this->getUser()) {
            throw new AccessDeniedException('You do not have permission to delete this reply');
        }
    
        // Remove the reply
        $entityManager->remove($reply);
        $entityManager->flush();
    
        return $this->redirectToRoute('blog_show', ['blog_slug' => $reply->getComment()->getBlog()->getSlug()]);
    }
    
    
}
