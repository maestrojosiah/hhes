<?php

namespace App\Controller;

use App\Entity\Constituency;
use App\Entity\County;
use App\Entity\Message;
use App\Entity\User;
use App\Form\ContactType;
use App\Form\UserProfileType;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use App\Repository\ConstituencyRepository;
use App\Repository\ContentRepository;
use App\Repository\CountyRepository;
use App\Repository\FAQRepository;
use App\Repository\ImageRepository;
use App\Repository\PageRepository;
use App\Repository\SEORepository;
use App\Repository\TestimonialsRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\PageDataService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Service\SeoService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class IndexController extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em, 
        private Mailer $mailer,
        private BookRepository $bookRepository,
        private BlogRepository $blogRepo,
        private FAQRepository $fAQRepository,
        private TestimonialsRepository $testimonialsRepository,
        private ContentRepository $contentRepository,
        private PageRepository $pageRepository,
        private ImageRepository $imageRepository,
        private CategoryRepository $bookCategoryRepository,
        private SEORepository $seoRepo,
        private HttpClientInterface $httpClient, 
        private SeoService $seoService, 
        private BookRepository $bookRepo, 
        private PageDataService $pageDataService
    ) {}

    #[Route('/', name: 'app_index')]
    public function index2(): Response
    {

        $banners = $this->bookRepo->findBy(
            ['banner' => true],
            ['id' => 'asc'],
            3
        );
        $health_books = $this->bookRepo->findByCategorySlug('health_wellness');
        $family_books = $this->bookRepo->findByCategorySlug('family');
        $children_books = $this->bookRepo->findByCategorySlug('children_books');
        $parenting_books = $this->bookRepo->findByCategorySlug('parenting');
        $blogPosts = $this->blogRepo->findBy(
            [],
            ['id' => 'desc'],
            4
        );

        $all_content = $this->contentRepository->findAll();
        $all_images = $this->imageRepository->findAll();
        $content = [];
        $images = [];
        $index = $this->pageRepository->findOneByUrl('home-page');
        $services = $this->pageRepository->findByIsService(true);
        $testimonials = $this->testimonialsRepository->findAll();
        $blog_posts = $this->blogRepo->findPublished(true, 3);
        $books = $this->bookRepository->findBy([], ['id' => 'desc'], 8);
        $categories = $this->bookCategoryRepository->findBy([], ['id' => 'desc'], 40);
        $faqs = $this->fAQRepository->findBy([], ['id' => 'asc'], 3);

        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        if (empty($categories)) {
            throw $this->createNotFoundException('No categories found');
        }

        $seo = $this->seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        $seo->setMetaTitle($index->getMetaTitle());
        $seo->setMetaDescription($index->getMetaDescription());

        // Pick a random category
        $randomCategory = $categories[array_rand($categories)];

        // Fetch books for the selected category
        $booksFromRandomCategory = $randomCategory->getBooks();

        // Shuffle and limit to 3 books
        $randomBooks = $booksFromRandomCategory->toArray();
        shuffle($randomBooks);
        $randomBooks = array_slice($randomBooks, 0, 3);
        
        return $this->render('index/index.html.twig', [
            'content' => $content,
            'page' => $index,
            'seo' => $seo,
            'images' => $images,
            'services' => $services,
            'testimonials' => $testimonials,
            'books' => $books,
            'categories' => $categories,
            'faqs' => $faqs,
            'featured_books' => $randomBooks,
            'random_category' => $randomCategory,
            'blog_posts' => $blog_posts,
            'banners' => $banners,
            'health_books' => $health_books,
            'family_books' => $family_books,
            'children_books' => $children_books,
            'parenting_books' => $parenting_books,
            'blogPosts' => $blogPosts

        ]);
    }

    #[Route('/p/{page}', name: 'app_page')]
    public function page(Request $request, string $page): Response
    {
        $pageData = $this->pageDataService->getPageData($request, $page);
        return $this->render('index/page.html.twig', $pageData);
    }


    #[Route('/profile', name: "app_profile_show")]
    public function profile(Request $request, EntityManagerInterface $em, CountyRepository $countyRepo){
        $counties = $countyRepo->findAll();
        $countyData = $this->getCountyData($counties);
        $user = $this->getUser();

        $user = $this->getUser();
        $form = $this->createForm(UserProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Profile updated successfully');

            return $this->redirectToRoute('app_profile_show');
        }


        return $this->render('index/profile.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'counties' => $counties,
            'countyData' => $countyData
        ]);
    }

    #[Route('/list/constituencies', name: "list_constituencies")]
    public function constituencies(Request $request, CountyRepository $countyRepo, $page = 'list/constituencies'){
        $counties = $countyRepo->findAll();
        $pageData = $this->pageDataService->getPageData($request, $page);
        $pageData['counties'] = $counties;
        return $this->render('index/page.html.twig', $pageData);
    }

    #[Route('/contact/us', name: "app_contact")]
    public function contact(Request $request, CountyRepository $countyRepo, $page = 'contact/us'){
        $counties = $countyRepo->findAll();
        $pageData = $this->pageDataService->getPageData($request, $page);
        $pageData['counties'] = $counties;
        return $this->render('index/page.html.twig', $pageData);
    }

    #[Route('/about/hhes/kenya', name: "app_about")]
    public function about(Request $request, $page = "about/hhes/kenya"){
        $pageData = $this->pageDataService->getPageData($request, $page);
        return $this->render('index/page.html.twig', $pageData);
    }

    #[Route('/faq', name: "app_faq")]
    public function faq(Request $request, $page = "faq"){
        $pageData = $this->pageDataService->getPageData($request, $page);
        return $this->render('index/page.html.twig', $pageData);
    }

    #[Route('/find/volunteer-community-educator', name: "app_find_vce")]
    public function findVce(CountyRepository $countyRepo, Request $request, $page = "find/volunteer-community-educator"){

        $counties = $countyRepo->createQueryBuilder('c')
        ->where('c.name LIKE :name')
        ->setParameter('name', '%County%')
        ->orderBy('c.id', 'DESC')
        ->getQuery()
        ->getResult();

        $countyData = $this->getCountyData($counties);

        $pageData = $this->pageDataService->getPageData($request, $page);

        $pageData['counties'] = $counties;
        $pageData['countyData'] = $countyData;

        return $this->render('index/page.html.twig', $pageData);

    }
    
    #[Route('/update/client/constituency', name: "update_constituency")]
    public function updateConstituency(
        Request $request,
        EntityManagerInterface $em,
        CountyRepository $countyRepository,
        ConstituencyRepository $constituencyRepository
    ): JsonResponse {
        $countyName = $request->request->get('county');
        $constituencyName = $request->request->get('constituency');

        if (!$countyName || !$constituencyName) {
            return new JsonResponse(['success' => false, 'message' => 'Invalid data provided.'], 400);
        }

        // Normalize the input
        $normalizedCountyName = strtolower(trim($countyName));

        // Find the best match
        $counties = $countyRepository->findAll();
        $county = null;

        foreach ($counties as $existingCounty) {
            // Normalize the database name
            $normalizedDatabaseName = strtolower(trim($existingCounty->getName()));

            // Check if the normalized input is contained in the normalized database name
            if (strpos($normalizedDatabaseName, $normalizedCountyName) !== false ||
                strpos($normalizedCountyName, $normalizedDatabaseName) !== false) {
                $county = $existingCounty;
                break;
            }
        }

        // Create a new County if no match is found
        if (!$county) {
            $county = new County();
            $county->setName($countyName); // Save the original name
            $em->persist($county);
        }

        // // Find or create County
        // $county = $countyRepository->findOneBy(['name' => $countyName]);
        // if (!$county) {
        //     $county = new County();
        //     $county->setName($countyName);
        //     $em->persist($county);
        // }

        // Find or create Constituency
        $constituency = $constituencyRepository->findOneBy(['name' => $constituencyName]);
        if (!$constituency) {
            $constituency = new Constituency();
            $constituency->setName($constituencyName);
            $constituency->setCounty($county);
            $em->persist($constituency);
        }

        // Save the entities
        $em->flush();

        return new JsonResponse([
            'success' => true,
            'constituencyId' => $constituency->getId(),
            'message' => 'Constituency and County updated successfully.'
        ]);
    }

    #[Route('/find/client/location', name: "get-location")]
    public function getLocation(Request $request, EntityManagerInterface $entityManager, CountyRepository $countyRepository, ConstituencyRepository $constituencyRepository): JsonResponse
    {
        $latitude = $request->query->get('lat');
        $longitude = $request->query->get('lng');
        
        if (!$latitude || !$longitude) {
            return new JsonResponse(['error' => 'Missing lat or lng parameters.'], 400);
        }
    
        // Step 1: Get the county using Google Maps API
        $apiKey = 'AIzaSyD98styKBPmzwxgazMFSy6uvTeImLFLPqQ';
        $googleApiUrl = sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=%s',
            $latitude,
            $longitude,
            $apiKey
        );
    
        $response = $this->httpClient->request('GET', $googleApiUrl);
        $googleData = $response->toArray();
    
        $county = null;
        if (!empty($googleData['results'])) {
            foreach ($googleData['results'][0]['address_components'] as $component) {
                if (in_array('administrative_area_level_1', $component['types'])) {
                    $county = $component['long_name'];
                    break;
                }
            }
        }
    
        if (!$county) {
            return new JsonResponse(['error' => 'Unable to determine county.'], 500);
        }
    
        // Step 2: Check if the county exists in the database
        $countyEntity = $countyRepository->findOneBy(['name' => $county]);
        if (!$countyEntity) {
            return new JsonResponse(['error' => 'County not found in the database.'], 500);
        }
    
        // Step 3: Load GeoJSON data and find the constituency
        $projDir = $this->getParameter('project_dir');
        $geojsonPath = $projDir . '/public/site/kenya.geojson';
    
        if (!file_exists($geojsonPath)) {
            return new JsonResponse(['error' => 'GeoJSON file not found.'], 500);
        }
    
        $geojsonData = json_decode(file_get_contents($geojsonPath), true);
    
        if (!$geojsonData) {
            return new JsonResponse(['error' => 'Unable to load GeoJSON data.'], 500);
        }
    
        $constituency = null;
        foreach ($geojsonData['features'] as $feature) {
            if ($this->isPointInPolygon($latitude, $longitude, $feature['geometry']['coordinates'][0])) {
                $constituency = $feature['properties']['shapeName'];
                break;
            }
        }
    
        if (!$constituency) {
            return new JsonResponse(['error' => 'Unable to determine constituency.'], 404);
        }
    
        // Step 4: Check if the constituency exists in the database, and if not, create it
        $constituencyEntity = $constituencyRepository->findOneBy(['name' => $constituency, 'county' => $countyEntity]);
        if (!$constituencyEntity) {
            // Create a new constituency entity if it doesn't exist
            $constituencyEntity = new Constituency();
            $constituencyEntity->setName($constituency);
            $constituencyEntity->setCounty($countyEntity);
    
            // Persist the new constituency to the database
            $entityManager->persist($constituencyEntity);
            $entityManager->flush();
        }
    
        // Step 5: Return both county and constituency
        return new JsonResponse([
            'county' => $county,
            'constituency' => $constituency,
            'constituency_id' => $constituencyEntity->getId(), // Optionally, return the ID of the constituency
        ]);
    }
        
    /**
     * Check if a point is inside a polygon using Ray-Casting algorithm.
     */
    private function isPointInPolygon(float $lat, float $lng, array $polygon): bool
    {
        $inside = false;
        $j = count($polygon) - 1;
        for ($i = 0; $i < count($polygon); $i++) {
            $xi = $polygon[$i][0];
            $yi = $polygon[$i][1];
            $xj = $polygon[$j][0];
            $yj = $polygon[$j][1];
    
            $intersect = (($yi > $lat) != ($yj > $lat)) &&
                ($lng < ($xj - $xi) * ($lat - $yi) / ($yj - $yi) + $xi);
            if ($intersect) {
                $inside = !$inside;
            }
            $j = $i;
        }
        return $inside;
    }
        
    public function getCountyData($counties){
        $countyData = [];
    
        foreach ($counties as $county) {
            $countyName = $county->getName();
            $constituencies = [];
    
            foreach ($county->getConstituencies() as $constituency) {
                $constituencies[] = $constituency->getName();
            }
    
            $countyData[$countyName] = $constituencies;
        }
    
        return $countyData;
    }
    
    #[Route(path: '/test/test/test', name: 'multitesting')]
    public function testingmultiplethings(): Response
    {

        $arr = ["composer.json", "composer.lock", "config/bundles.php", "config/packages/security.yaml", "config/services.yaml", "public/site/css/style.css", "src/Controller/Admin/AuthorCrudController.php", "src/Controller/Admin/BlogCategoryCrudController.php", "src/Controller/Admin/BlogCrudController.php", "src/Controller/Admin/BookCrudController.php", "src/Controller/Admin/CartCrudController.php", "src/Controller/Admin/CartItemCrudController.php", "src/Controller/Admin/CategoryCrudController.php", "src/Controller/Admin/CommentCrudController.php", "src/Controller/Admin/CommentRepliesCrudController.php", "src/Controller/Admin/ConstituencyCrudController.php", "src/Controller/Admin/CountyCrudController.php", "src/Controller/Admin/DashboardController.php", "src/Controller/Admin/MediaCrudController.php", "src/Controller/Admin/OrderHistoryCrudController.php", "src/Controller/Admin/OrderItemCrudController.php", "src/Controller/Admin/OrderrCrudController.php", "src/Controller/Admin/PreferenceCrudController.php", "src/Controller/Admin/PublisherCrudController.php", "src/Controller/Admin/TagCrudController.php", "src/Controller/Admin/UserCrudController.php", "src/Controller/BookController.php", "src/Controller/CartController.php", "src/Controller/RegistrationController.php", "src/Controller/ShopController.php", "src/Entity/User.php", "src/Form/UserProfileType.php", "src/Repository/BookRepository.php", "symfony.lock", "templates/base.html.twig", "templates/sections/shop_nav.html.twig", "templates/index/profile.html.twig", "templates/shop/grid.html.twig", "templates/shop/list.html.twig", "templates/tmp/header.html.twig", "src/Service/CsvExporter.php", "src/Service/WordExporter.php", "templates/admin/ckeditor_edit.html.twig", "templates/admin/ckeditor_new.html.twig", "templates/admin/fields/constituencies.html.twig", "templates/admin/fields/users.html.twig"];

        $proj_dir = $this->getParameter('project_dir');
        $test = [];
        foreach($arr as $a) {
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }

        return $this->render('index/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

    #[Route('/send/message/contact/form', name: 'contact_form')]
    public function contact_form(UserRepository $userRepo, Request $request): Response
    {

        // Create an instance of the form
        $form = $this->createForm(ContactType::class);
        
        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $name = $contact['name'];
            $email = $contact['email'];
            // $subject = $contact['subject'];
            $msg = $contact['message'];
            $date = new \DateTimeImmutable();

            $message = new Message();
            $message->setName($name);
            $message->setEmail($email);
            // $message->setSubject($subject);
            $message->setMessage($msg);
            $message->setReceivedOn($date);
            $this->em->persist($message);
            $this->em->flush();

            $subject = "Message From Contact Form";
            $admins = $userRepo->findByUsertype('admin');
            $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $msg];

            foreach ($admins as $admin) {
                $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
            }
            $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");
    
            $this->addFlash('success','Your Message has been sent');

        } else {

            foreach ($form->getErrors(true) as $error) {
                $this->addFlash('danger', $error->getMessage());
            }
        
        }
   
        return $this->redirectToRoute('app_index');

    }
    
    #[Route('/service/{service}', name: 'app_service')]
    public function service(ContentRepository $contentRepository, PageRepository $pageRepository, $service): Response
    {
        $all_content = $contentRepository->findAll();
        $content = [];
        $service_page = $pageRepository->findOneByUrl($service);
        $services = $pageRepository->findByIsService(true);
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }

       return $this->render('index/service.html.twig', [
            'content' => $content,
            'page' => $service_page,
            'services' => $services,
        ]);
    }

    #[Route('/post/{slug}', name: 'blog_detail')]
    public function blog(BlogCategoryRepository $blogCategoryRepository, BlogRepository $blogRepo, ImageRepository $imageRepository, ContentRepository $contentRepository, PageRepository $pageRepository, $slug): Response
    {
        $blog_post = $blogRepo->findOneBySlug($slug);
        $title = $blog_post->getTitle();
        $page_category = "Blog";
        $all_content = $contentRepository->findAll();
        $blog_categories = $blogCategoryRepository->findAll();
        $all_images = $imageRepository->findAll();
        $blog_posts = $blogRepo->findPublished(true, 3);
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }        

        $content = [];
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        
        $blog_page = $pageRepository->findOneByUrl('blog-content');
        $blog_page->setMetaDescription($blog_post->getMetaDescription());
        $blog_page->setMetaTitle($blog_post->getMetaTitle());

        $seo = $this->seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        $seo->setMetaTitle($blog_page->getMetaTitle());
        $seo->setMetaDescription($blog_page->getMetaDescription());
        
        return $this->render('index/page.html.twig', [
            'page_category' => $page_category,
            'blog_post' => $blog_post,
            'blog_posts' => $blog_posts,
            'page' => $blog_page,
            'content' => $content,
            'blog_categories' => $blog_categories,
            'images' => $images,
            'title' => $title,
            'seo' => $seo
        ]);
    }

    #[Route('/book/shop/{category}', name: 'shop', defaults: ['category' => null])]
    public function shop(
        SEORepository $seoRepo,
        BookRepository $bookRepository,
        ImageRepository $imageRepository,
        ContentRepository $contentRepository,
        PaginatorInterface $paginator,
        Request $request,
        PageRepository $pageRepository,
        $category
    ): Response {

        if (null !== $category) {
            // Query for books with a specific category
            $query = $bookRepository->createQueryBuilder('c')
                ->join('c.Categories', 'cat')
                ->where('cat.slug = :category')
                ->setParameter('category', $category)
                ->getQuery();
        } else {
            // Query for all books without filtering by category
            $query = $bookRepository->createQueryBuilder('c')
                ->getQuery();
        }
    
        // Paginate the results
        $books = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), // Current page
            12 // Items per page
        );
        
        // $categoryEntity = $this->bookCategoryRepository->findOneBySlug($category);
        // $title = ucfirst($categoryEntity->getName()) . " Books – Same-Day Delivery";
        // $page_category = ucfirst($categoryEntity->getName());

        // Fetch books that belong to the specified category
        // $books = $bookRepository->findByCategorySlug($category);
        $bookCategory = $this->bookCategoryRepository->findOneBySlug($category);
        $categories = $this->bookCategoryRepository->findBy([], ['id' => 'desc'], 40);

        // Fetch related books (not necessarily by category)
        $related_books = $bookRepository->findBy(
            [],
            ['id' => 'desc'],
            4
        );
        
        $cat_page = $pageRepository->findOneByUrl('shop');

        $seo = $seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        if(null !== $category){
            $title = ucfirst($bookCategory->getName()) . " Nairobi – Same-Day Delivery";
            $page_category = ucfirst($bookCategory->getName());
            $cat_page->setMetaDescription($bookCategory->getMetaDescription());
            $cat_page->setMetaTitle($bookCategory->getMetaTitle());    
            $seo->setMetaTitle($bookCategory->getMetaTitle());
            $seo->setMetaDescription($bookCategory->getMetaDescription());
    
        } else {
            $title = $cat_page->getHeading();
            $page_category = "All Books";
            $seo->setMetaTitle($cat_page->getMetaTitle());
            $seo->setMetaDescription($cat_page->getMetaDescription());    
        }
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }
    
        $content = [];
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        
        $keywords = $seo->getKeywords();
        foreach ($books as $book) {
            $keywords .= ", " . $book->getName();
        }
        $seo->setKeywords($keywords);
        $testimonials = $this->testimonialsRepository->findAll();

        return $this->render('index/page.html.twig', [
            'books' => $books,
            'page_category' => $page_category,
            'related_books' => $related_books,
            'page' => $cat_page,
            'category' => $bookCategory,
            'categories' => $categories,
            'content' => $content,
            'testimonials' => $testimonials,
            'images' => $images,
            'seo' => $seo,
            'title' => $title,
        ]);
    }

    public function renderFooterForm(): Response
    {
        $form = $this->createForm(ContactType::class);
        
        return $this->render('tmp/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}
