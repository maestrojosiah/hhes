<?php

namespace App\Controller;

use App\Entity\OrderItem;
use App\Form\AddToCartType;
use App\Manager\CartManager;
use App\Repository\BookRepository;
use App\Repository\SEORepository;
use App\Service\SeoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class BookController extends AbstractController
{

    public function __construct(private readonly SeoService $seoService, private readonly EntityManagerInterface $em, private SEORepository $seoRepo)
    {}


    #[Route('/book/{slug}', name: 'show_book', requirements: ['slug' => '[a-zA-Z0-9\-]+'])]
    public function index(BookRepository $bookRepo, Request $request, CartManager $cartManager, $slug): Response
    {
        $book = $bookRepo->findOneBy(
            ['slug' => $slug, 'banner' => false, 'availability' => 'available', 'deleted' => false],
            ['id' => 'desc']
        );

        $this->seoService->setTitle($book->getTitle())
            ->setDescription(substr(strip_tags($book->getDescription()), 0, 165))
            ->setKeywords('HHES Kenya, Home Health Education Service, health books, educational resources, family health, spiritual growth, Swahili books, English books, relationship books, mental health books, book delivery Kenya')
            ->setOgTitle($book->getTitle())
            ->setOgDescription(substr(strip_tags($book->getDescription()), 0, 165))
            ->setOgImage('https://hheskenya.org/site/images/books/'.$book->getImage())
            ->setOgUrl('https://hheskenya.org/')
            ->setOgType('website');

        $slugs = [];
        $categories = $book->getCategory();
        foreach ($categories as $key => $c) {
            $slugs[] = $c->getSlug();
        }
        $related_books = $bookRepo->findRandomBooksByCategorySlugs($slugs, 3);

        $form = $this->createForm(AddToCartType::class, null, [
            'book'=> $book,
        ]);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $clickedButton = $request->request->get('clickedButton'); // Get the button name from AJAX data
                
            $item = $form->getData();

            // calculate tax
            $tax = 0;

            $item->setBook($book);
            $item->setPricePerUnit($book->getCost());
            $item->setTotalPrice($item->calculateTotalPrice());
            $item->setTaxAmount($tax);
            $item->setSubtotal($item->calculateSubtotal());
            // $this->em->persist($item);
            // $item->setBookVariant();
            // $item->setIsGift();
            // $item->setGiftMessage();
            // $item->setNotes();
            $item->setShippingStatus('processing');
            if(null !== $this->getUser()) {
                $item->setUser($this->getUser());
            }
            
            // $item->setDeliveryDate();
            // $item->setPaymentDate();
    
            
            $cart = $cartManager->getCurrentCart();

            $currentDate = date('ymd');
            // Create the tracking number by concatenating the date and order ID
            $trackingNumber = $currentDate .'-'. rand(1000, 9999);
            $cart
                ->addOrderItem($item)
                ->setOrderDate(new \DateTime())
                ->setPaymentMethod('mpesa_on_delivery')
                // ->setTotalAmount('') this will be set at the last stage of checkout
                // ->setTaxAmount
                // ->setShippingAmount
                // ->setSubtotal
                // ->setDiscountAmount
                ->setTrackingNumber($trackingNumber)
                // ->setShippingMethod
                ->setPaymentStatus('pending')
                // ->setPaymentDate
                // ->setNotes
                // ->setPromoCode
                ->setDeliveryStatus('not_shipped')
                // ->setOrderDate
                // ->setQuantity
                ;

            if(null !== $this->getUser()) {
                $cart->setUser($this->getUser());
            }
            $cartManager->save($cart);
            $totalBooks = count($cart->getOrderItems());
            $orderItems = $cart->getOrderItems();
            $totalPrice = $cart->getTotal();

            $books = [];
            foreach($orderItems as $orderItem) {
                $books[] = ['name'=> $orderItem->getBook()->getTitle(),'quantity'=> $orderItem->getQuantity(), 'total' => $orderItem->getTotal(), 'images' => $orderItem->getBook()->getImage(), 'slug' => $orderItem->getBook()->getSlug()];
            }
            // // return $this->redirectToRoute('book_show', ['book_slug' => $book->getSlug()]);
            if ($clickedButton === 'add_to_cart[add]') {
                // 'Add' button was clicked
                // Your logic for 'Add' button here
                return new JsonResponse(['status' => 'success', 'message' => 'add', 'totalBooks' => $totalBooks,'totalPrice'=> $totalPrice, 'books' => $books]);
            } elseif ($clickedButton === 'add_to_cart[addNBuy]') {
                // 'Buy Now' button was clicked
                // Your logic for 'Buy Now' button here
                return new JsonResponse(['status' => 'success', 'message' => 'addNBuy']);
            } else {
                return new JsonResponse(['status' => 'Failed', 'message' => 'I dont know what happened']);
            }

        }

        $seo = $this->seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );
        
        $authorsCollection = $book->getAuthor(); // This is a Doctrine PersistentCollection

        // Convert PersistentCollection to an array and extract names
        $authorsArray = $authorsCollection->toArray();
        $authorNames = array_map(fn($author) => $author->getName(), $authorsArray); // Adjust method as needed
        
        // Concatenate authors into a string
        $authorString = implode(", ", $authorNames);
        
        $seo->setMetaTitle($book->getTitle() . " by " . $authorString);
        $seo->setMetaDescription(substr(strip_tags($book->getDescription()), 0, 157) . "...");

        // dd($this->seoService->generateMetaTags());
        return $this->render('book/show.html.twig', [
            'form'=> $form->createView(),
            'book' => $book,
            'title' => $book->getTitle(),
            'related_books' => $related_books,
            'meta_tags' => $this->seoService->generateMetaTags(),
        ]);
    }

    #[Route('/add/product/to/cart/index', name: 'add_product_to_cart_index')]
    public function addToCartFromIndex(BookRepository $bookRepo, CartManager $cartManager, Request $request)
    {

        $product_id = $request->request->get('product_id');
        $clickedButton = $request->request->get('clickedButton');


        $product = $bookRepo->findOneById($product_id);

        $clickedButton = $request->request->get('clickedButton'); // Get the button name from AJAX data
            
        $item = new OrderItem();

        // if returning customer, give discount
        // calculate tax
        $tax = 0;

        $item->setQuantity(1);
        $item->setBook($product);
        $item->setPricePerUnit($product->getCost());
        $item->setTotalPrice($item->calculateTotalPrice());
        $item->setTaxAmount($tax);
        $item->setSubtotal($item->calculateSubtotal());
        // $item->setIsGift();
        // $item->setGiftMessage();
        // $item->setNotes();
        $item->setShippingStatus('processing');
        if(null !== $this->getUser()) {
            $item->setUser($this->getUser());
        }
        
        $this->em->persist($item);
        $this->em->flush();

        // $item->setDeliveryDate();
        // $item->setPaymentDate();

        
        $cart = $cartManager->getCurrentCart();

        $currentDate = date('ymd');
        // Create the tracking number by concatenating the date and order ID
        $trackingNumber = $currentDate .'-'. rand(1000, 9999);
        $cart
            ->addOrderItem($item)
            ->setOrderDate(new \DateTime())
            ->setPaymentMethod('mpesa')
            // ->setTotalAmount('') this will be set at the last stage of checkout
            // ->setTaxAmount
            // ->setShippingAmount
            // ->setSubtotal
            // ->setDiscountAmount
            ->setTrackingNumber($trackingNumber)
            // ->setShippingMethod
            ->setPaymentStatus('pending')
            // ->setPaymentDate
            // ->setNotes
            // ->setPromoCode
            ->setDeliveryStatus('not_shipped')
            // ->setOrderDate
            // ->setQuantity
            ;

        if(null !== $this->getUser()) {
            $cart->setUser($this->getUser());
        }
        $cartManager->save($cart);

        return new JsonResponse(['status' => 'success', 'message' => 'addNBuy']);

    }


}
