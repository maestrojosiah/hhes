<?php 

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\FormLoginAuthenticator;

class RegistrationController extends AbstractController
{
    private $authenticator;
    private $userAuthenticator;

    public function __construct(UserAuthenticatorInterface $userAuthenticator, FormLoginAuthenticator $authenticator)
    {
        $this->userAuthenticator = $userAuthenticator;
        $this->authenticator = $authenticator;
    }

    #[Route('/security/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Honeypot check
            $honeypotValue = $form->get('middlename')->getData();
            if (!empty($honeypotValue)) {
                return $this->redirect('https://example.com'); // Redirect to example.com
            }

            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setUsertype('customer');
            $user->setRoles(['ROLE_CUSTOMER']);

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Welcome! You successfully created an account.'
            );

            // Authenticate the user
            $this->userAuthenticator->authenticateUser(
                $user,
                $this->authenticator,
                $request
            );

            return $this->redirectToRoute('checkout');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form,
        ]);
    }
}
