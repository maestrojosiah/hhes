<?php

namespace App\Storage;

use App\Entity\Orderr;
use App\Repository\OrderrRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class CartSessionStorage
{
    /**
     * The request stack.
     *
     * @var RequestStack
     */
    private $requestStack;

    /**
     * The cart repository.
     *
     * @var OrderrRepository
     */
    private $cartRepository;
    private $tokenStorage;
    private $security;

    /**
     * @var string
     */
    const CART_KEY_NAME = 'cart_id';

    /**
     * CartSessionStorage constructor.
     *
     * @param RequestStack $requestStack
     * @param OrderrRepository $cartRepository
     * @param TokenStorageInterface $tokenStorage
     * @param Security $tokenStorage
     */
    public function __construct(RequestStack $requestStack, TokenStorageInterface $tokenStorage, OrderrRepository $cartRepository, Security $security)
    {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->cartRepository = $cartRepository;
        $this->security = $security;
    }

    // /**
    //  * Gets the cart in session.
    //  *
    //  * @return Orderr|null
    //  */
    // public function getCart(): ?Orderr
    // {
    //     return $this->cartRepository->findOneBy([
    //         'id' => $this->getCartId(),
    //         'order_status' => Orderr::STATUS_CART
    //     ]);
    // }
    /**
     * Gets the cart for the current user.
     *
     * @return Orderr|null
     */
    public function getCart(): ?Orderr
    {
        $user = $this->getUserFromSecurityContext();

        if ($user) {

            // If a user is logged in, try to get the cart associated with the user
            $userCart = $this->cartRepository->findOneBy([
                'user' => $user,
                'order_status' => Orderr::STATUS_CART
            ]);

            // If a cart is associated with the user, return it
            if ($userCart) {
                return $userCart;
            }
        } else {
            // echo "<pre> there's no user logged in";
            // print_r($user->getId());
            // die();
    
        }

        // If no user is logged in or no cart is associated with the user, get the cart from session
        return $this->cartRepository->findOneBy([
            'id' => $this->getCartId(),
            'order_status' => Orderr::STATUS_CART
        ]);
    }

     /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }

    /**
     * Sets the cart in session for a specific user.
     *
     * @param Orderr $cart
     * @param UserInterface|null $user
     */
    public function setCart(Orderr $cart, ?UserInterface $user = null): void
    {
        if(!$user){
            $this->getSession()->set(self::CART_KEY_NAME, $cart->getId());
        } else {
            $this->clearCart();
        }
        

        // echo "<pre>when setting cart in cartSessionStorage";
        // var_dump($cart->getId());
        // die();

        if ($user) {
            $cart->setUser($user);
        }
    }

    public function clearCart()
    {
        $this->getSession()->remove(self::CART_KEY_NAME);

        // Rest of your controller logic
    }

    /**
     * Returns the cart id.
     *
     * @return int|null
     */
    private function getCartId(): ?int
    {
        return $this->getSession()->get(self::CART_KEY_NAME);
    }

    private function getSession(): SessionInterface
    {
        return $this->requestStack->getSession();
    }
}
