<?php

namespace App\Factory;

use App\Entity\Orderr;
use App\Entity\OrderItem;
use App\Entity\Book;

/**
 * Class OrderFactory
 * @package App\Factory
 */
class OrderFactory
{
    /**
     * Creates an Orderr.
     *
     * @return Orderr
     */
    public function create(): Orderr
    {
        $order = new Orderr();
        $order
            ->setOrderStatus(Orderr::STATUS_CART)
            ->setOrderDate(new \DateTime());
            
        return $order;
    }

    /**
     * Creates an item for a book.
     *
     * @param Book $book
     *
     * @return OrderItem
     */
    public function createItem(Book $book): OrderItem
    {
        $item = new OrderItem();
        $item->setBook($book);
        $item->setQuantity(1);

        return $item;
    }
}