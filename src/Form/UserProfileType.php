<?php 

// src/Form/UserProfileType.php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;
use App\Entity\County;
use App\Entity\Constituency;

class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('f_name', TextType::class, [
                'label' => 'First Name',
                'required' => false
            ])
            ->add('l_name', TextType::class, [
                'label' => 'Last Name',
                'required' => false
            ])
            ->add('username', TextType::class, [
                'label' => 'Username',
                'required' => false
            ])
            ->add('residence', TextType::class, [
                'label' => 'Residence',
                'required' => false
            ])
            ->add('email', TextType::class, [
                'label' => 'Email Address',
                'required' => false
            ])
            ->add('phone', TextType::class, [
                'label' => 'Contact Number',
                'required' => false
            ])
            ->add('county', EntityType::class, [
                'class' => County::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true, // Set to true for checkboxes
                'label' => 'County',
                'required' => false
            ])
            ->add('constituency', EntityType::class, [
                'class' => Constituency::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true, // Set to true for checkboxes
                'label' => 'Constituency',
                'required' => false
            ])
            ->add('about', TextareaType::class, [
                'label' => 'About you',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
