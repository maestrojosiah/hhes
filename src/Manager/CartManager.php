<?php

namespace App\Manager;

use App\Entity\CartMerge;
use App\Entity\Orderr;
use App\Factory\OrderFactory;
use App\Repository\CartMergeRepository;
use App\Repository\CartRepository;
use App\Repository\OrderrRepository;
use App\Storage\CartSessionStorage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CartManager
 * @package App\Manager
 */
class CartManager
{
    /**
     * @var CartSessionStorage
     */
    private $cartSessionStorage;

    /**
     * @var OrderFactory
     */
    private $cartFactory;
    private $em;
    private $cartRepository;
    private $cartMergeRepository;

    /**
     * CartManager constructor.
     *
     * @param CartSessionStorage $cartStorage
     * @param OrderFactory $orderFactory
     * @param OrderrRepository $cartRepository
     */
    public function __construct(
        CartSessionStorage $cartStorage,
        OrderFactory $orderFactory,
        OrderrRepository $cartRepository,
        EntityManagerInterface $em,
        CartMergeRepository $cartMergeRepository
    ) {
        $this->cartSessionStorage = $cartStorage;
        $this->cartFactory = $orderFactory;
        $this->cartRepository = $cartRepository;
        $this->em = $em;   
        $this->cartMergeRepository = $cartMergeRepository;
    }


    /**
     * Gets the current cart for a specific user.
     *
     * @param UserInterface|null $user
     * @return Orderr
     */
    public function getCurrentCart(?UserInterface $user = null): Orderr
    {

        // If a user is logged in, try to get the cart associated with the user
        if ($user) {

            $cartToMerge = $this->cartMergeRepository->findOneByEmail($user->getEmail());
            $userCart = $this->cartRepository->findOneBy([
                'user' => $user,
                'order_status' => Orderr::STATUS_CART
            ]);

            // If there is a cart in the session, merge it with the user's cart
            if ($cartToMerge) {
                // echo "<pre> if cart to merge";
                // print_r($cartToMerge->getId());
                // die();
                $sessionCart = $this->cartRepository->find($cartToMerge->getCartId());
                if($userCart){
                    $this->mergeCarts($userCart, $sessionCart, $user);
                    $this->cartSessionStorage->setCart($userCart, $user);
                } else {
                    $this->cartSessionStorage->setCart($sessionCart, $user);
                }

                // Clear the session cart after merging
                $this->deleteCartMerges($user->getEmail());
            }

            // If a cart is associated with the user, check for a session cart
            if ($userCart) {
                // find if there is a cart from session in the cartMerge table
                return $userCart;
            }
        }

        // If no user is logged in or no cart is associated with the user, get the cart from session
        $cart = $this->cartSessionStorage->getCart();

        // If no cart is found in session, create a new one
        if (!$cart) {
            $cart = $this->cartFactory->create();

            if ($user) {
                $cart->setUser($user);
            }
        }

        return $cart;

    }

    private function deleteCartMerges(string $email): void
    {
        $cartMerges = $this->cartMergeRepository->findByEmail($email);
        foreach ($cartMerges as $cartMerge) {
            $this->em->remove($cartMerge);
            $this->em->flush();
        }
    }
    /**
     * Merges two carts.
     *
     * @param Orderr $destinationCart
     * @param Orderr $sourceCart
     * @param UserInterface|null $user
     * @param EntityManagerInterface $entityManager
     */
    private function mergeCarts(Orderr $destinationCart, Orderr $sourceCart, ?UserInterface $user = null): void
    {

        // echo "<pre> in the mergeCarts function";
        // print_r($destinationCart->getId());
        // print_r($sourceCart->getId());
        // die();
        // Set the user on the destination cart if provided
        if ($user) {
            $destinationCart->setUser($user);
            $this->em->persist($destinationCart);
            $this->em->flush();
        }

        foreach ($sourceCart->getOrderItems() as $sourceItem) {
            // Check if a similar item exists in the destination cart
            // Set the user on each destination item if provided
            if ($user) {
                $sourceItem->setUser($user);
                $this->em->persist($sourceItem);
                $this->em->flush();
            }

            $destinationItem = $destinationCart->findItemByBook($sourceItem->getBook());
    
            if ($destinationItem) {
                // If the item exists, increase the quantity
                $newQuantity = $destinationItem->getQuantity() + $sourceItem->getQuantity();
                $destinationItem->setQuantity($newQuantity);
                $destinationItem->setTotalPrice($destinationItem->getBook()->getCost() + $newQuantity);
                $destinationItem->setSubtotal($destinationItem->getBook()->getCost() + $newQuantity);
                $this->em->persist($destinationItem);
                $this->em->flush();

            } else {
                // If the item does not exist, add it to the destination cart
                $destinationCart->addOrderItem($sourceItem);
            }

            
        }

        
        
    
    }


    /**
     * Persists the cart in the database and session for a specific user.
     *
     * @param Orderr $cart
     * @param UserInterface|null $user
     */
    public function save(Orderr $cart, ?UserInterface $user = null): void
    {
        if ($user) {
            $cart->setUser($user);
        }

        // echo "<pre>when saving order in cart manaager";
        // var_dump($user->getId());
        // die();

        // Persist in database
        $this->em->persist($cart);
        $this->em->flush();

        // Persist in session
        $this->cartSessionStorage->setCart($cart, $user);
    }
}

