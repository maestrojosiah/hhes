<?php 

namespace App\Service;

use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\KernelInterface;

class WordExporter
{
    public function __construct(private KernelInterface $kernel)
    {}

    public function createResponseFromQueryBuilder(QueryBuilder $queryBuilder, FieldCollection $fields, string $filename)
    {
        $result = $queryBuilder->getQuery()->getArrayResult();

        // Columns to omit
        $columnsToOmit = ['password', 'id', 'slug', 'isbn', 'language', 'uploaded', 'deleted', 'banner', 'featured', 'format'];  // Add more column keys as needed

        // Column widths (in twips, 1 twip = 1/20 of a point)
        $columnWidths = [
            'title' => 3000,
            'slug' => 2000,
            'isbn' => 2000,
            'description' => 8000,
            'publisher' => 3000,
            'language' => 2000,
            'image' => 3000,
            'user' => 3000,
            'cost' => 2000,
            'availability' => 2000,
            'uploaded' => 3000,
            'deleted' => 2000,
            'banner' => 2000,
            'featured' => 2000,
            'format' => 2000
        ];

        // Convert datetime objects into strings
        $data = [];
        // $arr = [];
        foreach ($result as $index => $row) {
            foreach ($row as $columnKey => $columnValue) {
                if (in_array($columnKey, $columnsToOmit)) {
                    // Skip omitted columns
                    continue;
                }
                // $arr[] = $columnKey;
                if (gettype($columnValue) == 'array') {
                    $columnValue = implode(',', $columnValue);
                }

                $data[$index][$columnKey] = $columnValue instanceof \DateTimeInterface
                    ? $columnValue->format('Y-m-d H:i:s')
                    : $columnValue;
            }
        }
        // echo "<pre>";
        // print_r($arr);
        // die();
        // Create a new PHPWord object
        $phpWord = new PhpWord();
        $section = $phpWord->addSection();

        // Add table to the document
        $table = $section->addTable();

        // Add headers
        if (isset($data[0])) {
            $headers = [];
            $properties = array_keys($data[0]);
            foreach ($properties as $property) {
                $headers[$property] = ucfirst($property);
                foreach ($fields as $field) {
                    if ($property === $field->getProperty() && $field->getLabel()) {
                        $headers[$property] = $field->getLabel();
                        break;
                    }
                }
            }

            // Add header row to the table
            $table->addRow();
            foreach ($headers as $header) {
                $table->addCell($columnWidths[$header] ?? 2000)->addText($header);
            }

            $projectDir = $this->kernel->getProjectDir();
            foreach ($data as $row) {
                $table->addRow();
                foreach ($headers as $key => $header) {
                    $cell = $table->addCell($columnWidths[$key]);
                    if ($key == 'image') {
                        $imagePath = $projectDir . '/public/site/images/books/' . $row[$key];
                        if ($imagePath && file_exists($imagePath)) {
                            $cell->addImage($imagePath, [
                                'width' => 100,
                            ]);
                        } else {
                            // Handle the case where the image does not exist
                            $cell->addText('Image not found');
                        }
                    } else {
                        $cell->addText($row[$key] ?? '');
                    }
                }
            }
        }

        $response = new StreamedResponse(function () use ($phpWord, $filename) {
            $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save('php://output');
        });

        $dispositionHeader = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );

        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', true, 200);

        return $response;
    }
}
