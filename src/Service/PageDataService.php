<?php

namespace App\Service;

use App\Repository\BookRepository;
use App\Repository\TestimonialsRepository;
use App\Repository\BlogRepository;
use App\Repository\FAQRepository;
use App\Repository\ContentRepository;
use App\Repository\PageRepository;
use App\Repository\ImageRepository;
use App\Repository\SEORepository;
use Symfony\Component\HttpFoundation\Request;

class PageDataService
{
    public function __construct(
        private BookRepository $bookRepository,
        private TestimonialsRepository $testimonialsRepository,
        private BlogRepository $blogRepo,
        private FAQRepository $faqRepository,
        private ContentRepository $contentRepository,
        private PageRepository $pageRepository,
        private ImageRepository $imageRepository,
        private SEORepository $seoRepo
    ) {}

    public function getPageData(Request $request, string $page): array
    {
        $bookId = $request->query->get('bookId');
        $book = $bookId ? $this->bookRepository->find($bookId) : null;

        $books = $this->bookRepository->findAll();
        $allContent = $this->contentRepository->findAll();
        $pageEntity = $this->pageRepository->findOneByUrl($page);
        $services = $this->pageRepository->findByIsService(true);
        $testimonials = $this->testimonialsRepository->findAll();
        $allImages = $this->imageRepository->findAll();
        $faqs = $this->faqRepository->findAll();
        $blogPosts = $this->blogRepo->findPublished(true, 50);
        $seo = $this->seoRepo->findOneBy([], ['id' => 'asc']);

        if ($pageEntity) {
            $seo->setMetaTitle($pageEntity->getMetaTitle());
            $seo->setMetaDescription($pageEntity->getMetaDescription());
        }

        // Process images
        $images = [];
        foreach ($allImages as $item) {
            $images[$item->getSlug()] = $item;
        }

        // Process content
        $content = [];
        foreach ($allContent as $item) {
            $content[$item->getDescription()] = $item;
        }

        return [
            'page_category' => 'Pages',
            'content' => $content,
            'page' => $pageEntity,
            'books' => $books,
            'services' => $services,
            'images' => $images,
            'title' => $pageEntity ? $pageEntity->getHeading() : '',
            'faqs' => $faqs,
            'testimonials' => $testimonials,
            'blog_posts' => $blogPosts,
            'seo' => $seo,
            'book' => $book,
        ];
    }
}
