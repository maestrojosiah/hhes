<?php

namespace App\Entity;

use App\Repository\BlogCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BlogCategoryRepository::class)]
class BlogCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $category_name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image_url = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_active = null;

    #[ORM\Column(nullable: true)]
    private ?int $display_order = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'parent_category')]
    private ?self $parentCategory = null;

    #[ORM\OneToMany(mappedBy: 'parentCategory', targetEntity: self::class)]
    private Collection $parent_category;

    #[ORM\ManyToMany(targetEntity: Blog::class, inversedBy: 'blogCategories')]
    private Collection $blog;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    // #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'parent_category')]
    // private ?self $blogCategory = null;

    public function __construct()
    {
        $this->parent_category = new ArrayCollection();
        $this->blog = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getCategoryName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryName(): ?string
    {
        return $this->category_name;
    }

    public function setCategoryName(?string $category_name): static
    {
        $this->category_name = $category_name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(?string $image_url): static
    {
        $this->image_url = $image_url;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(?bool $is_active): static
    {
        $this->is_active = $is_active;

        return $this;
    }

    public function getDisplayOrder(): ?int
    {
        return $this->display_order;
    }

    public function setDisplayOrder(?int $display_order): static
    {
        $this->display_order = $display_order;

        return $this;
    }

    public function getParentCategory(): ?self
    {
        return $this->parentCategory;
    }

    public function setParentCategory(?self $parentCategory): static
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    public function addParentCategory(self $parentCategory): static
    {
        if (!$this->parent_category->contains($parentCategory)) {
            $this->parent_category->add($parentCategory);
            $parentCategory->setParentCategory($this);
        }

        return $this;
    }

    public function removeParentCategory(self $parentCategory): static
    {
        if ($this->parent_category->removeElement($parentCategory)) {
            // set the owning side to null (unless already changed)
            if ($parentCategory->getParentCategory() === $this) {
                $parentCategory->setParentCategory(null);
            }
        }

        return $this;
    }

    // public function getBlogCategory(): ?self
    // {
    //     return $this->blogCategory;
    // }

    // public function setBlogCategory(?self $blogCategory): static
    // {
    //     $this->blogCategory = $blogCategory;

    //     return $this;
    // }

    /**
     * @return Collection<int, Blog>
     */
    public function getBlog(): Collection
    {
        return $this->blog;
    }

    public function addBlog(Blog $blog): static
    {
        if (!$this->blog->contains($blog)) {
            $this->blog->add($blog);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): static
    {
        $this->blog->removeElement($blog);

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }
}
