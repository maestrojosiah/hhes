<?php

namespace App\Entity;

use App\Repository\CountyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountyRepository::class)]
class County
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'county')]
    private Collection $users;

    /**
     * @var Collection<int, Constituency>
     */
    #[ORM\OneToMany(targetEntity: Constituency::class, mappedBy: 'county')]
    private Collection $constituencies;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->constituencies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addCounty($this);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        if ($this->users->removeElement($user)) {
            $user->removeCounty($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Constituency>
     */
    public function getConstituencies(): Collection
    {
        return $this->constituencies;
    }

    public function addConstituency(Constituency $constituency): static
    {
        if (!$this->constituencies->contains($constituency)) {
            $this->constituencies->add($constituency);
            $constituency->setCounty($this);
        }

        return $this;
    }

    public function removeConstituency(Constituency $constituency): static
    {
        if ($this->constituencies->removeElement($constituency)) {
            // set the owning side to null (unless already changed)
            if ($constituency->getCounty() === $this) {
                $constituency->setCounty(null);
            }
        }

        return $this;
    }
}
