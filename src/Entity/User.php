<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_EMAIL', fields: ['email'])]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private ?string $email = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $f_name = null;

    #[ORM\Column(length: 255)]
    private ?string $l_name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $username = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $residence = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $category = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $usertype = null;

    #[ORM\Column(nullable: true)]
    public ?bool $is_active = null;

    /**
     * @var Collection<int, Book>
     */
    #[ORM\OneToMany(targetEntity: Book::class, mappedBy: 'user')]
    private Collection $books;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $gender = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $profile_picture = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $registration_date = null;

    /**
     * @var Collection<int, Presentation>
     */
    #[ORM\OneToMany(targetEntity: Presentation::class, mappedBy: 'user')]
    private Collection $presentations;


    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Cart::class)]
    private Collection $carts;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OrderItem::class)]
    private Collection $orderItems;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Address::class)]
    private Collection $addresses;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OrderHistory::class, orphanRemoval: true)]
    private Collection $orderHistories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Orderr::class)]
    private Collection $orderrs;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Preference::class)]
    private Collection $preferences;

    /**
     * @var Collection<int, County>
     */
    #[ORM\ManyToMany(targetEntity: County::class, inversedBy: 'users')]
    private Collection $county;

    /**
     * @var Collection<int, Constituency>
     */
    #[ORM\ManyToMany(targetEntity: Constituency::class, inversedBy: 'users')]
    private Collection $constituency;

    #[ORM\Column(type: 'boolean')]
    private bool $profileComplete = false;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $about = null;

    /**
     * @var Collection<int, UserRating>
     */
    #[ORM\OneToMany(targetEntity: UserRating::class, mappedBy: 'customer')]
    private Collection $customerRatings;

    /**
     * @var Collection<int, UserRating>
     */
    #[ORM\OneToMany(targetEntity: UserRating::class, mappedBy: 'agent')]
    private Collection $agentRatings;

    /**
     * @var Collection<int, Orderr>
     */
    #[ORM\OneToMany(targetEntity: Orderr::class, mappedBy: 'agent')]
    private Collection $customer_orders;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Blog::class)]
    private Collection $blogs;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Comment::class)]
    private Collection $comments;

    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->orderHistories = new ArrayCollection();
        $this->orderrs = new ArrayCollection();
        $this->carts = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
        $this->presentations = new ArrayCollection();
        $this->preferences = new ArrayCollection();
        $this->county = new ArrayCollection();
        $this->constituency = new ArrayCollection();
        $this->customerRatings = new ArrayCollection();
        $this->agentRatings = new ArrayCollection();
        $this->customer_orders = new ArrayCollection();
        $this->blogs = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getFullName();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFName(): ?string
    {
        return $this->f_name;
    }

    public function setFName(string $f_name): static
    {
        $this->f_name = $f_name;

        return $this;
    }

    public function getLName(): ?string
    {
        return $this->l_name;
    }

    public function setLName(string $l_name): static
    {
        $this->l_name = $l_name;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->f_name .' '. $this->l_name;
    }

    public function getAvatarUri(): ?string
    {
        $photoUri = $this->profile_picture;
        if(null == $photoUri){
            return "user.jpg";
        } else {
            return $photoUri;
        }
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getResidence(): ?string
    {
        return $this->residence;
    }

    public function setResidence(?string $residence): static
    {
        $this->residence = $residence;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function getUsertype(): ?string
    {
        return $this->usertype;
    }

    public function setUsertype(string $usertype): static
    {
        $this->usertype = $usertype;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->is_active;
    }

    public function setActive(bool $is_active): static
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * @return Collection<int, Book>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): static
    {
        if (!$this->books->contains($book)) {
            $this->books->add($book);
            $book->setUser($this);
        }

        return $this;
    }

    public function removeBook(Book $book): static
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getUser() === $this) {
                $book->setUser(null);
            }
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    public function getProfilePicture(): ?string
    {
        return $this->profile_picture;
    }

    public function setProfilePicture(?string $profile_picture): static
    {
        $this->profile_picture = $profile_picture;

        return $this;
    }

    public function getRegistrationDate(): ?\DateTimeInterface
    {
        return $this->registration_date;
    }

    public function setRegistrationDate(?\DateTimeInterface $registration_date): static
    {
        $this->registration_date = $registration_date;

        return $this;
    }

    /**
     * @return Collection<int, Presentation>
     */
    public function getPresentations(): Collection
    {
        return $this->presentations;
    }

    public function addPresentation(Presentation $presentation): static
    {
        if (!$this->presentations->contains($presentation)) {
            $this->presentations->add($presentation);
            $presentation->setUser($this);
        }

        return $this;
    }

    public function removePresentation(Presentation $presentation): static
    {
        if ($this->presentations->removeElement($presentation)) {
            // set the owning side to null (unless already changed)
            if ($presentation->getUser() === $this) {
                $presentation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Address>
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): static
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses->add($address);
            $address->setUser($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): static
    {
        if ($this->addresses->removeElement($address)) {
            // set the owning side to null (unless already changed)
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderHistory>
     */
    public function getOrderHistories(): Collection
    {
        return $this->orderHistories;
    }

    public function addOrderHistory(OrderHistory $orderHistory): static
    {
        if (!$this->orderHistories->contains($orderHistory)) {
            $this->orderHistories->add($orderHistory);
            $orderHistory->setUser($this);
        }

        return $this;
    }

    public function removeOrderHistory(OrderHistory $orderHistory): static
    {
        if ($this->orderHistories->removeElement($orderHistory)) {
            // set the owning side to null (unless already changed)
            if ($orderHistory->getUser() === $this) {
                $orderHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Orderr>
     */
    public function getOrderrs(): Collection
    {
        return $this->orderrs;
    }

    public function addOrderr(Orderr $orderr): static
    {
        if (!$this->orderrs->contains($orderr)) {
            $this->orderrs->add($orderr);
            $orderr->setUser($this);
        }

        return $this;
    }

    public function removeOrderr(Orderr $orderr): static
    {
        if ($this->orderrs->removeElement($orderr)) {
            // set the owning side to null (unless already changed)
            if ($orderr->getUser() === $this) {
                $orderr->setUser(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Cart>
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): static
    {
        if (!$this->carts->contains($cart)) {
            $this->carts->add($cart);
            $cart->setUser($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): static
    {
        if ($this->carts->removeElement($cart)) {
            // set the owning side to null (unless already changed)
            if ($cart->getUser() === $this) {
                $cart->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setUser($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getUser() === $this) {
                $orderItem->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Preference>
     */
    public function getPreferences(): Collection
    {
        return $this->preferences;
    }

    public function addPreference(Preference $preference): static
    {
        if (!$this->preferences->contains($preference)) {
            $this->preferences->add($preference);
            $preference->setUser($this);
        }

        return $this;
    }

    public function removePreference(Preference $preference): static
    {
        if ($this->preferences->removeElement($preference)) {
            // set the owning side to null (unless already changed)
            if ($preference->getUser() === $this) {
                $preference->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, County>
     */
    public function getCounty(): Collection
    {
        return $this->county;
    }

    public function addCounty(County $county): static
    {
        if (!$this->county->contains($county)) {
            $this->county->add($county);
        }

        return $this;
    }

    public function removeCounty(County $county): static
    {
        $this->county->removeElement($county);

        return $this;
    }

    /**
     * @return Collection<int, Constituency>
     */
    public function getConstituency(): Collection
    {
        return $this->constituency;
    }

    public function addConstituency(Constituency $constituency): static
    {
        if (!$this->constituency->contains($constituency)) {
            $this->constituency->add($constituency);
        }

        return $this;
    }

    public function removeConstituency(Constituency $constituency): static
    {
        $this->constituency->removeElement($constituency);

        return $this;
    }

    // Getter and setter for profileComplete
    public function isProfileComplete(): bool
    {
        return $this->profileComplete;
    }

    public function setProfileComplete(bool $profileComplete): self
    {
        $this->profileComplete = $profileComplete;
        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function checkProfileComplete(): void
    {
        $this->updateProfileComplete();
    }

    public function updateProfileComplete(): void
    {
        $isComplete = $this->f_name && $this->l_name && $this->email && $this->phone && $this->profile_picture && $this->usertype && $this->gender && $this->category;
        $this->profileComplete = $isComplete;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): static
    {
        $this->about = $about;

        return $this;
    }

    /**
     * @return Collection<int, UserRating>
     */
    public function getCustomerRatings(): Collection
    {
        return $this->customerRatings;
    }

    public function addCustomerRating(UserRating $customerRating): static
    {
        if (!$this->customerRatings->contains($customerRating)) {
            $this->customerRatings->add($customerRating);
            $customerRating->setCustomer($this);
        }

        return $this;
    }

    public function removeCustomerRating(UserRating $customerRating): static
    {
        if ($this->customerRatings->removeElement($customerRating)) {
            // set the owning side to null (unless already changed)
            if ($customerRating->getCustomer() === $this) {
                $customerRating->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserRating>
     */
    public function getAgentRatings(): Collection
    {
        return $this->agentRatings;
    }

    public function addAgentRating(UserRating $agentRating): static
    {
        if (!$this->agentRatings->contains($agentRating)) {
            $this->agentRatings->add($agentRating);
            $agentRating->setAgent($this);
        }

        return $this;
    }

    public function removeAgentRating(UserRating $agentRating): static
    {
        if ($this->agentRatings->removeElement($agentRating)) {
            // set the owning side to null (unless already changed)
            if ($agentRating->getAgent() === $this) {
                $agentRating->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Orderr>
     */
    public function getCustomerOrders(): Collection
    {
        return $this->customer_orders;
    }

    public function addCustomerOrder(Orderr $customerOrder): static
    {
        if (!$this->customer_orders->contains($customerOrder)) {
            $this->customer_orders->add($customerOrder);
            $customerOrder->setAgent($this);
        }

        return $this;
    }

    public function removeCustomerOrder(Orderr $customerOrder): static
    {
        if ($this->customer_orders->removeElement($customerOrder)) {
            // set the owning side to null (unless already changed)
            if ($customerOrder->getAgent() === $this) {
                $customerOrder->setAgent(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, Blog>
     */
    public function getBlogs(): Collection
    {
        return $this->blogs;
    }

    public function addBlog(Blog $blog): static
    {
        if (!$this->blogs->contains($blog)) {
            $this->blogs->add($blog);
            $blog->setAuthor($this);
        }

        return $this;
    }

    public function removeBlog(Blog $blog): static
    {
        if ($this->blogs->removeElement($blog)) {
            // set the owning side to null (unless already changed)
            if ($blog->getAuthor() === $this) {
                $blog->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

}
