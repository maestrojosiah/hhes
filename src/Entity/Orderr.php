<?php

namespace App\Entity;

use App\Repository\OrderrRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderrRepository::class)]
class Orderr
{
    const STATUS_CART = "cart";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orderrs')]
    private ?User $user = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $order_status = self::STATUS_CART;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $payment_method = null;

    #[ORM\Column(nullable: true)]
    private ?float $total_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $tax_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $shipping_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $subtotal = null;

    #[ORM\Column(nullable: true)]
    private ?float $discount_amount = null;

    #[ORM\Column(nullable: true)]
    private ?string $tracking_number = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shipping_method = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $payment_status = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $payment_date = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $promo_code = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $delivery_status = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $delivery_date = null;

    #[ORM\OneToMany(mappedBy: 'orderr', targetEntity: OrderItem::class, cascade: ['persist','remove'], orphanRemoval:true)]
    private Collection $orderItems;

    #[ORM\ManyToOne(inversedBy: 'orderrs')]
    private ?Address $shippingAddress = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $orderDate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $express = null;

    #[ORM\ManyToOne(inversedBy: 'customer_orders')]
    private ?User $agent = null;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->tracking_number;
    }
    /**
     * Find an item in the cart based on the associated book.
     *
     * @param Book $book
     * @return OrderItem|null
     */
    public function findItemByBook(Book $book): ?OrderItem
    {
        foreach ($this->orderItems as $item) {
            // Assuming $item->getBook() returns the associated book
            if ($item->getBook() === $book) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Calculates the order total.
     *
     * @return float
     */
    public function getTotal(): float
    {
        $total = 0;

        foreach ($this->getOrderItems() as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }

    public function calculateTotalAmount(): ?float
    {
        $totalAmount = 0;

        /** @var OrderItem $orderItem */
        foreach ($this->orderItems as $orderItem) {
            $totalAmount += $orderItem->calculateTotalPrice() ?? 0;
        }

        // Add shipping amount, if set
        if ($this->shipping_amount !== null) {
            $totalAmount += $this->shipping_amount;
        }

        // Add tax amount, if set
        if ($this->tax_amount !== null) {
            $totalAmount += $this->tax_amount;
        }

        // Subtract discount amount, if set
        if ($this->discount_amount !== null) {
            $totalAmount -= $this->discount_amount;
        }

        return $totalAmount;
    }

    public function calculateSubtotal(): ?float
    {
        $subtotal = 0;

        /** @var OrderItem $orderItem */
        foreach ($this->orderItems as $orderItem) {
            $subtotal += $orderItem->calculateSubtotal() ?? 0;
        }

        return $subtotal;
    }
        
    public function calculateTotalTaxAmount(): ?float
    {
        $totalTaxAmount = 0;

        /** @var OrderItem $orderItem */
        foreach ($this->orderItems as $orderItem) {
            if ($orderItem->getTaxAmount() !== null) {
                $totalTaxAmount += $orderItem->getTaxAmount();
            }
        }

        // Add tax amount from the order itself, if set
        if ($this->tax_amount !== null) {
            $totalTaxAmount += $this->tax_amount;
        }

        return $totalTaxAmount;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getOrderStatus(): ?string
    {
        return $this->order_status;
    }

    public function setOrderStatus(?string $order_status): static
    {
        $this->order_status = $order_status;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->payment_method;
    }

    public function setPaymentMethod(?string $payment_method): static
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    public function getTotalAmount(): ?float
    {
        return $this->calculateTotalAmount();
    }

    public function setTotalAmount(?float $total_amount): static
    {
        $this->total_amount = $total_amount;

        return $this;
    }

    public function getTaxAmount(): ?float
    {
        return $this->tax_amount;
    }

    public function setTaxAmount(?float $tax_amount): static
    {
        $this->tax_amount = $tax_amount;

        return $this;
    }

    public function getShippingAmount(): ?float
    {
        return $this->shipping_amount;
    }

    public function setShippingAmount(?float $shipping_amount): static
    {
        $this->shipping_amount = $shipping_amount;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(?float $subtotal): static
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discount_amount;
    }

    public function setDiscountAmount(?float $discount_amount): static
    {
        $this->discount_amount = $discount_amount;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(?string $tracking_number): static
    {
        $this->tracking_number = $tracking_number;

        return $this;
    }

    public function getShippingMethod(): ?string
    {
        return $this->shipping_method;
    }

    public function setShippingMethod(?string $shipping_method): static
    {
        $this->shipping_method = $shipping_method;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->payment_status;
    }

    public function setPaymentStatus(?string $payment_status): static
    {
        $this->payment_status = $payment_status;

        return $this;
    }

    public function getPaymentDate(): ?\DateTimeInterface
    {
        return $this->payment_date;
    }

    public function setPaymentDate(?\DateTimeInterface $payment_date): static
    {
        $this->payment_date = $payment_date;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getPromoCode(): ?string
    {
        return $this->promo_code;
    }

    public function setPromoCode(?string $promo_code): static
    {
        $this->promo_code = $promo_code;

        return $this;
    }

    public function getDeliveryStatus(): ?string
    {
        return $this->delivery_status;
    }

    public function setDeliveryStatus(?string $delivery_status): static
    {
        $this->delivery_status = $delivery_status;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->delivery_date;
    }

    public function setOrderDate(?\DateTimeInterface $orderDate): static
    {
        $this->orderDate = $orderDate;

        return $this;
    }    
    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        foreach ($this->getOrderItems() as $existingItem) {
            // The item already exists, update the quantity
            if ($existingItem->equals($orderItem)) {
                $existingItem->setQuantity(
                    $existingItem->getQuantity() + $orderItem->getQuantity()
                );
                return $this;
            }
        }
    
        $this->orderItems[] = $orderItem;
        $orderItem->setOrderr($this);
    
        return $this;
    }
    
    // public function addOrderItem(OrderItem $orderItem): static
    // {
    //     foreach ($this->getOrderItems() as $existingOrderItem) {

    //         if (!$this->orderItems->contains($orderItem)) {
    //             $this->orderItems->add($orderItem);
    //             $orderItem->setOrderr($this);
    //         }
    
    //     }

    //     return $this;
    // }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrderr() === $this) {
                $orderItem->setOrderr(null);
            }
        }

        return $this;
    }

    /**
     * Removes all items from the order.
     *
     * @return $this
     */
    public function removeItems(): self
    {
        foreach ($this->getOrderItems() as $item) {
            $this->removeOrderItem($item);
        }

        return $this;
    }


    public function getShippingAddress(): ?Address
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(?Address $shippingAddress): static
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getOrderDate(): ?\DateTimeInterface
    {
        return $this->orderDate;
    }

    public function isExpress(): ?bool
    {
        return $this->express;
    }

    public function setExpress(?bool $express): static
    {
        $this->express = $express;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): static
    {
        $this->agent = $agent;

        return $this;
    }

}
