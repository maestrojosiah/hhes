<?php

namespace App\Entity;

use App\Repository\PreferenceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PreferenceRepository::class)]
class Preference
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'preferences')]
    private ?User $user = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $language = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $notification_settings = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $communication_pref = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $privacy_settings = null;

    #[ORM\OneToOne(inversedBy: 'preference', cascade: ['persist', 'remove'])]
    private ?Address $default_shipping_address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email_preference = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $marketing_consent = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->getUser()->getFullName();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): static
    {
        $this->language = $language;

        return $this;
    }

    public function getNotificationSettings(): ?string
    {
        return $this->notification_settings;
    }

    public function setNotificationSettings(?string $notification_settings): static
    {
        $this->notification_settings = $notification_settings;

        return $this;
    }

    public function getCommunicationPref(): ?string
    {
        return $this->communication_pref;
    }

    public function setCommunicationPref(?string $communication_pref): static
    {
        $this->communication_pref = $communication_pref;

        return $this;
    }

    public function getPrivacySettings(): ?string
    {
        return $this->privacy_settings;
    }

    public function setPrivacySettings(?string $privacy_settings): static
    {
        $this->privacy_settings = $privacy_settings;

        return $this;
    }

    public function getDefaultShippingAddress(): ?Address
    {
        return $this->default_shipping_address;
    }

    public function setDefaultShippingAddress(?Address $default_shipping_address): static
    {
        $this->default_shipping_address = $default_shipping_address;

        return $this;
    }

    public function getEmailPreference(): ?string
    {
        return $this->email_preference;
    }

    public function setEmailPreference(?string $email_preference): static
    {
        $this->email_preference = $email_preference;

        return $this;
    }

    public function getMarketingConsent(): ?string
    {
        return $this->marketing_consent;
    }

    public function setMarketingConsent(?string $marketing_consent): static
    {
        $this->marketing_consent = $marketing_consent;

        return $this;
    }
}
