<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @extends ServiceEntityRepository<Book>
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findByCategorySlug(string $slug, int $limit = 10): array
    {
        $query = $this->createQueryBuilder('b')
            ->innerJoin('b.category', 'c')
            ->where('c.slug = :slug')
            ->andWhere('b.banner = :banner')
            ->andWhere('b.deleted = :deleted')
            ->andWhere('b.availability = :availability')
            ->setParameter('slug', $slug)
            ->setParameter('banner', false)
            ->setParameter('deleted', false)
            ->setParameter('availability', 'available')
            ->getQuery();

            $entities = $query->getResult();

            if (count($entities) < $limit) {
                $limit = count($entities); // Limit cannot be greater than total entities
            }
    
            // Shuffle a copy of the array for randomness
            $shuffledEntities = array_slice($entities, 0); // Create a copy
            shuffle($shuffledEntities);
    
            return array_slice($shuffledEntities, 0, $limit);
    }
    
    public function findByBookCategoriesQuery(array $categories): Query
    {
        return $this->createQueryBuilder('b')
            ->join('b.category', 'c')  // assuming 'categories' is the name of the property in the Book entity
            ->where('c.id IN (:category)')  // or 'c.name' if you are using category names
            ->setParameter('category', $categories)
            ->getQuery();
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('b')
            ->getQuery();
    }

    public function findByCategorySlugs(array $slugs): array
    {
        return $this->createQueryBuilder('b')
            ->innerJoin('b.category', 'c')
            ->where('c.slug IN (:slugs)')
            ->andWhere('b.banner = :banner')
            ->andWhere('b.deleted = :deleted')
            ->andWhere('b.availability = :availability')
            ->setParameter('slugs', $slugs)
            ->setParameter('banner', false)
            ->setParameter('deleted', false)
            ->setParameter('availability', 'available')
            ->getQuery()
            ->getResult();
    }

    public function findRandomBooksByCategorySlugs(array $slugs, int $limit = 3): array
    {
        $query = $this->createQueryBuilder('b')
            ->innerJoin('b.category', 'c')
            ->where('c.slug IN (:slugs)')
            ->andWhere('b.banner = :banner')
            ->andWhere('b.deleted = :deleted')
            ->andWhere('b.availability = :availability')
            ->setParameter('slugs', $slugs)
            ->setParameter('banner', false)
            ->setParameter('deleted', false)
            ->setParameter('availability', 'available')
            ->getQuery();

        $entities = $query->getResult();

        if (count($entities) < $limit) {
            $limit = count($entities); // Limit cannot be greater than total entities
        }

        // Shuffle a copy of the array for randomness
        $shuffledEntities = array_slice($entities, 0); // Create a copy
        shuffle($shuffledEntities);

        return array_slice($shuffledEntities, 0, $limit);
    }

    
    public function searchBooks(string $query, string $categorySlug = null)
    {
        $qb = $this->createQueryBuilder('b')
                   ->leftJoin('b.category', 'c')
                   ->leftJoin('b.author', 'a')
                   ->addSelect('c', 'a')
                   ->where('b.banner = :banner AND (b.title LIKE :query OR a.name LIKE :query)')
                    ->andWhere('b.deleted = :deleted')
                    ->andWhere('b.availability = :availability')
                   ->setParameter('query', '%' . $query . '%')
                   ->setParameter('banner', false)
                    ->setParameter('deleted', false)
                    ->setParameter('availability', 'available');

        if ($categorySlug) {
            $qb->andWhere('c.slug = :categorySlug')
               ->setParameter('categorySlug', $categorySlug);
        }

        return $qb->getQuery()->getResult();
    }
    
    //    /**
    //     * @return Book[] Returns an array of Book objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Book
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
