<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function findAgents($county, $constituency, int $limit = 10)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.usertype = :usertype')
            ->andWhere('u.is_active = :active')
            ->andWhere('u.phone IS NOT NULL')
            ->andWhere('u.category = :category')
            ->setParameter('usertype', 'user')
            ->setParameter('active', true)
            ->setParameter('category', 'le');
    
        if ($constituency) {
            $qb->andWhere(':constituency MEMBER OF u.constituency')
               ->setParameter('constituency', $constituency);
        } elseif ($county) {
            $qb->andWhere(':county MEMBER OF u.county')
               ->setParameter('county', $county);
        }
    
        $agents = $qb->orderBy('u.profileComplete', 'ASC') // Prioritize complete profiles
                    ->getQuery()
                    ->getResult();
    
        if (empty($agents)) {
            // If no agents found, fetch all admins
            $agents = $this->createQueryBuilder('u')
                ->where('u.roles LIKE :adminRole')
                ->setParameter('adminRole', '%ROLE_ADMIN%')
                ->getQuery()
                ->getResult();
        }
    
        if (count($agents) < $limit) {
            $limit = count($agents); // Limit cannot be greater than total agents
        }
    
        // Shuffle the agents for randomness
        $shuffledAgents = array_slice($agents, 0); // Create a copy of the array
        shuffle($shuffledAgents);
    
        return array_slice($shuffledAgents, 0, $limit);
    }
    

    //    /**
    //     * @return User[] Returns an array of User objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('u.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?User
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
